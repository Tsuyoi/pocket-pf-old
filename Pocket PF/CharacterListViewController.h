//
//  CharacterListViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Character.h"
#import "CharacterListCell.h"
#import "CharacterDetailViewController.h"

@interface CharacterListViewController : UITableViewController <CharacterDetailViewControllerDelegate>

@property (strong, nonatomic) NSMutableArray * characters;
@property (strong, nonatomic) NSManagedObjectContext * context;

@end
