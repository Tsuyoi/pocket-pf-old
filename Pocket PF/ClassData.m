//
//  ClassData.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "ClassData.h"
#import "CharacterData.h"


@implementation ClassData

@dynamic alchemist;
@dynamic barbarian;
@dynamic bard;
@dynamic cavalier;
@dynamic cleric;
@dynamic druid;
@dynamic fighter;
@dynamic gunslinger;
@dynamic inquisitor;
@dynamic magus;
@dynamic monk;
@dynamic oracle;
@dynamic paladin;
@dynamic ranger;
@dynamic rogue;
@dynamic sorcerer;
@dynamic summoner;
@dynamic witch;
@dynamic wizard;
@dynamic character;

@end
