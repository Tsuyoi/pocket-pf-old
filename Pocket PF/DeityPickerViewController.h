//
//  DeityPickerViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 3/31/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DeityPickerViewController;

@protocol DeityPickerViewControllerDelegate <NSObject>

- (void)deityPickerViewController:(DeityPickerViewController *)controller didSelectDeity:(NSNumber *)deityIndex;

@end

@interface DeityPickerViewController : UITableViewController

@property (nonatomic, weak) id <DeityPickerViewControllerDelegate> delegate;
@property (nonatomic, strong) NSNumber * deity;

@end
