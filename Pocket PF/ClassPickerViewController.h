//
//  ClassPickerViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 2/13/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  ClassPickerViewController;

@protocol ClassPickerViewControllerDelegate <NSObject>

- (void)classPickerViewController:(ClassPickerViewController *)controller didSelectClass:(NSString *)className classIndex:(int)index;

@end

@interface ClassPickerViewController : UITableViewController

@property (weak, nonatomic) id <ClassPickerViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString * className;

@end
