//
//  SpellSource.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "SpellSource.h"


@implementation SpellSource

@dynamic alchemist;
@dynamic area;
@dynamic bard;
@dynamic casting_time;
@dynamic cleric;
@dynamic components;
@dynamic desc;
@dynamic dismissible;
@dynamic domain;
@dynamic druid;
@dynamic duration;
@dynamic effect;
@dynamic inquisitor;
@dynamic magus;
@dynamic name;
@dynamic oracle;
@dynamic paladin;
@dynamic range;
@dynamic ranger;
@dynamic saving_throw;
@dynamic school;
@dynamic short_desc;
@dynamic sor;
@dynamic spell_level;
@dynamic spell_resist;
@dynamic subschool;
@dynamic summoner;
@dynamic targets;
@dynamic witch;
@dynamic wiz;

@end
