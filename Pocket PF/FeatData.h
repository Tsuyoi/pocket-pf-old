//
//  FeatData.h
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CharacterData;

@interface FeatData : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * prereq;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) CharacterData *character;

@end
