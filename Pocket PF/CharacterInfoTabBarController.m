//
//  CharacterInfoTabBarController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 3/30/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "CharacterInfoTabBarController.h"
#import "CharacterLevelUpViewController.h"

@implementation CharacterInfoTabBarController {
    int totalSkillRanks;
    int skillRanksSpent;
}

- (void)saveContext {
    NSError * error = nil;
    NSManagedObjectContext * managedObjectContext = self.context;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Failed to save to data store: %@", [error localizedDescription]);
            NSArray * detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
            if (detailedErrors != nil && [detailedErrors count] > 0) {
                for (NSError * detailedError in detailedErrors) {
                    NSLog(@"  Detailed Error: %@", [detailedError userInfo]);
                }
            } else {
                NSLog(@"  %@", [error userInfo]);
            }
        }
    }
}
- (void)refreshSkillsBadge {
    totalSkillRanks = 0;
    int intMod = (self.character.intel.intValue / 2) - 5;
    totalSkillRanks += self.classes.alchemist.intValue * (4 + intMod);
    totalSkillRanks += self.classes.barbarian.intValue * (4 + intMod);
    totalSkillRanks += self.classes.bard.intValue * (6 + intMod);
    totalSkillRanks += self.classes.cavalier.intValue * (4 + intMod);
    totalSkillRanks += self.classes.cleric.intValue * (2 + intMod);
    totalSkillRanks += self.classes.druid.intValue * (4 + intMod);
    totalSkillRanks += self.classes.fighter.intValue * (2 + intMod);
    totalSkillRanks += self.classes.gunslinger.intValue * (4 + intMod);
    totalSkillRanks += self.classes.inquisitor.intValue * (6 + intMod);
    totalSkillRanks += self.classes.magus.intValue * (2 + intMod);
    totalSkillRanks += self.classes.monk.intValue * (4 + intMod);
    totalSkillRanks += self.classes.oracle.intValue * (4 + intMod);
    totalSkillRanks += self.classes.paladin.intValue * (2 + intMod);
    totalSkillRanks += self.classes.ranger.intValue * (6 + intMod);
    totalSkillRanks += self.classes.rogue.intValue * (8 + intMod);
    totalSkillRanks += self.classes.sorcerer.intValue * (2 + intMod);
    totalSkillRanks += self.classes.summoner.intValue * (2 + intMod);
    totalSkillRanks += self.classes.witch.intValue * (2 + intMod);
    totalSkillRanks += self.classes.wizard.intValue * (2 + intMod);
    
    skillRanksSpent = 0;
    skillRanksSpent += self.skills.acrobatics.intValue;
    skillRanksSpent += self.skills.appraise.intValue;
    skillRanksSpent += self.skills.bluff.intValue;
    skillRanksSpent += self.skills.climb.intValue;
    skillRanksSpent += self.skills.diplomacy.intValue;
    skillRanksSpent += self.skills.disable.intValue;
    skillRanksSpent += self.skills.disguise.intValue;
    skillRanksSpent += self.skills.escape.intValue;
    skillRanksSpent += self.skills.fly.intValue;
    skillRanksSpent += self.skills.handle.intValue;
    skillRanksSpent += self.skills.heal.intValue;
    skillRanksSpent += self.skills.intimidate.intValue;
    skillRanksSpent += self.skills.know_arcana.intValue;
    skillRanksSpent += self.skills.know_dungeon.intValue;
    skillRanksSpent += self.skills.know_engine.intValue;
    skillRanksSpent += self.skills.know_geography.intValue;
    skillRanksSpent += self.skills.know_history.intValue;
    skillRanksSpent += self.skills.know_local.intValue;
    skillRanksSpent += self.skills.know_nature.intValue;
    skillRanksSpent += self.skills.know_nobility.intValue;
    skillRanksSpent += self.skills.know_planes.intValue;
    skillRanksSpent += self.skills.know_religion.intValue;
    skillRanksSpent += self.skills.linguistics.intValue;
    skillRanksSpent += self.skills.perception.intValue;
    skillRanksSpent += self.skills.ride.intValue;
    skillRanksSpent += self.skills.sense.intValue;
    skillRanksSpent += self.skills.sleight.intValue;
    skillRanksSpent += self.skills.spellcraft.intValue;
    skillRanksSpent += self.skills.stealth.intValue;
    skillRanksSpent += self.skills.survival.intValue;
    skillRanksSpent += self.skills.swim.intValue;
    skillRanksSpent += self.skills.use_magic.intValue;
    if (totalSkillRanks - skillRanksSpent > 0)
        [[[[self viewControllers] objectAtIndex:1] tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%d", totalSkillRanks - skillRanksSpent]];
    else
        [[[[self viewControllers] objectAtIndex:1] tabBarItem] setBadgeValue:nil];
}

@synthesize character;
@synthesize classes;
@synthesize feats;
@synthesize spells;
@synthesize skills;
@synthesize context;
@synthesize levelUpButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self refreshSkillsBadge];    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"LevelUp"]) {
        CharacterLevelUpViewController * levelUp = segue.destinationViewController;
        levelUp.parent = self;
    }
}

/*- (void)pressedLevelUp:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Test!" message:@"You've hit the Level Up button" delegate:self cancelButtonTitle:@"Not Now" otherButtonTitles:@"Okay", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            NSLog(@"Cancel");
            break;
        case 1:
            NSLog(@"Okay");
            break;
            
        default:
            NSLog(@"Default");
            break;
    }
}*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
