//
//  CharacterData.h
//  Pocket PF
//
//  Created by Caylin Hickey on 5/1/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ClassData, FeatData, MagicItemData, SkillData, SpellData, WeaponData;

@interface CharacterData : NSManagedObject

@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSNumber * alignment;
@property (nonatomic, retain) NSNumber * cha;
@property (nonatomic, retain) NSNumber * cls;
@property (nonatomic, retain) NSNumber * con;
@property (nonatomic, retain) NSNumber * currhp;
@property (nonatomic, retain) NSNumber * deity;
@property (nonatomic, retain) NSNumber * dex;
@property (nonatomic, retain) NSNumber * exp;
@property (nonatomic, retain) NSString * eyes;
@property (nonatomic, retain) NSNumber * female;
@property (nonatomic, retain) NSNumber * gold;
@property (nonatomic, retain) NSString * hair;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSString * homeland;
@property (nonatomic, retain) NSNumber * hp;
@property (nonatomic, retain) NSNumber * intel;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * race;
@property (nonatomic, retain) NSNumber * str;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSNumber * wis;
@property (nonatomic, retain) ClassData *classes;
@property (nonatomic, retain) NSSet *feats;
@property (nonatomic, retain) MagicItemData *magicitems;
@property (nonatomic, retain) SkillData *skills;
@property (nonatomic, retain) NSSet *spells;
@property (nonatomic, retain) NSSet *weapons;
@end

@interface CharacterData (CoreDataGeneratedAccessors)

- (void)addFeatsObject:(FeatData *)value;
- (void)removeFeatsObject:(FeatData *)value;
- (void)addFeats:(NSSet *)values;
- (void)removeFeats:(NSSet *)values;

- (void)addSpellsObject:(SpellData *)value;
- (void)removeSpellsObject:(SpellData *)value;
- (void)addSpells:(NSSet *)values;
- (void)removeSpells:(NSSet *)values;

- (void)addWeaponsObject:(WeaponData *)value;
- (void)removeWeaponsObject:(WeaponData *)value;
- (void)addWeapons:(NSSet *)values;
- (void)removeWeapons:(NSSet *)values;

@end
