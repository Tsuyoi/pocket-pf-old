//
//  CharacterListViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "CharacterBasicInfoTabViewController.h"
#import "CharacterSkillTableViewController.h"
#import "CharacterSpellTableViewController.h"
#import "CharacterInfoTabBarController.h"
#import "CharacterListViewController.h"
#import "CharacterData.h"
#import "ClassData.h"
#import "FeatData.h"
#import "FeatSource.h"
#import "SkillData.h"
#import "SpellData.h"
#import "SpellSource.h"
#import "Helper.h"

@implementation CharacterListViewController

- (void)saveContext {
    NSError * error = nil;
    NSManagedObjectContext * managedObjectContext = self.context;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Failed to save to data store: %@", [error localizedDescription]);
            NSArray * detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
            if (detailedErrors != nil && [detailedErrors count] > 0) {
                for (NSError * detailedError in detailedErrors) {
                    NSLog(@"  Detailed Error: %@", [detailedError userInfo]);
                }
            } else {
                NSLog(@"  %@", [error userInfo]);
            }
        }
    }
}

@synthesize characters;
@synthesize context;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.characters count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CharacterListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SavedCharacter"];
    CharacterData * rowChar = [self.characters objectAtIndex:indexPath.row];
    cell.nameLabel.text = rowChar.name;
    NSString * race = [Helper resolveRaceToString:rowChar.race];
    NSString * gender = [Helper resolveGenderToString:rowChar.female];
    NSString * class = [Helper resolveClassToString:rowChar.cls];
    cell.descriptionLabel.text = [NSString stringWithFormat:@"%@, %@ %@", gender, race, class];
    cell.classImage.image = [Helper resolveClassToImage:rowChar.cls];
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddCharacter"]) {
        UINavigationController * navController = segue.destinationViewController;
        CharacterDetailViewController * charDetailViewController = [[navController viewControllers] objectAtIndex:0];
        charDetailViewController.delegate = self;
    }
    if ([segue.identifier isEqualToString:@"ViewCharacter"]) {
        CharacterInfoTabBarController * tabController = segue.destinationViewController;
        CharacterBasicInfoTabViewController * basicInfoTab = [[tabController viewControllers] objectAtIndex:0];
        CharacterSkillTableViewController * skillTab = [[tabController viewControllers] objectAtIndex:1];
        CharacterSpellTableViewController * spellTab = [[tabController viewControllers] objectAtIndex:2];
        NSIndexPath * indexPath = [[self tableView] indexPathForSelectedRow];
        tabController.context = context;
        tabController.character = [[self characters] objectAtIndex:indexPath.row];
        tabController.classes = [[[self characters] objectAtIndex:indexPath.row] classes];
        tabController.feats = [[[self characters] objectAtIndex:indexPath.row] feats];
        tabController.spells = [[[self characters] objectAtIndex:indexPath.row] spells];
        tabController.skills = [[[self characters] objectAtIndex:indexPath.row] skills];
        basicInfoTab.parent = tabController;
        skillTab.parent = tabController;
        skillTab.charIndex = indexPath.row;
        spellTab.parent = tabController;
        spellTab.charIndex = indexPath.row;
    }
}

#pragma mark - CharacterDetailViewControllerDelegate
- (void)characterDetailViewControllerDidCancel:(CharacterDetailViewController *)controller {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (void)characterDetailViewController:(CharacterDetailViewController *)controller didAddCharacter:(Character *)character {
    NSError * error;
    
    CharacterData * Char = [NSEntityDescription insertNewObjectForEntityForName:@"CharacterData" inManagedObjectContext:context];
    
    Char.name = character.name;
    Char.alignment = [NSNumber numberWithInt:character.alignment];
    Char.female = [NSNumber numberWithInt:character.gender];
    Char.race = [NSNumber numberWithInt:character.race];
    Char.cha = [NSNumber numberWithInt:character.cha];
    Char.con = [NSNumber numberWithInt:character.con];
    Char.dex = [NSNumber numberWithInt:character.dex];
    Char.intel = [NSNumber numberWithInt:character.intel];
    Char.str = [NSNumber numberWithInt:character.str];
    Char.wis = [NSNumber numberWithInt:character.wis];
    Char.cls = [NSNumber numberWithInt:character.cls];
    
    ClassData * Cls = [NSEntityDescription insertNewObjectForEntityForName:@"ClassData" inManagedObjectContext:context];
    switch (character.cls) {
        case 0:
            Cls.barbarian = [NSNumber numberWithInt:1];
            break;
        case 1:
            Cls.bard = [NSNumber numberWithInt:1];
            break;
        case 2:
            Cls.cleric = [NSNumber numberWithInt:1];
            break;
        case 3:
            Cls.druid = [NSNumber numberWithInt:1];
            break;
        case 4:
            Cls.fighter = [NSNumber numberWithInt:1];
            break;
        case 5:
            Cls.monk = [NSNumber numberWithInt:1];
            break;
        case 6:
            Cls.paladin = [NSNumber numberWithInt:1];
            break;
        case 7:
            Cls.ranger = [NSNumber numberWithInt:1];
            break;
        case 8:
            Cls.rogue = [NSNumber numberWithInt:1];
            break;
        case 9:
            Cls.sorcerer = [NSNumber numberWithInt:1];
            break;
        case 10:
            Cls.wizard = [NSNumber numberWithInt:1];
            break;
        case 11:
            Cls.alchemist = [NSNumber numberWithInt:1];
            break;
        case 12:
            Cls.cavalier = [NSNumber numberWithInt:1];
            break;
        case 13:
            Cls.gunslinger = [NSNumber numberWithInt:1];
            break;
        case 14:
            Cls.inquisitor = [NSNumber numberWithInt:1];
            break;
        case 15:
            Cls.magus = [NSNumber numberWithInt:1];
            break;
        case 16:
            Cls.oracle = [NSNumber numberWithInt:1];
            break;
        case 17:
            Cls.summoner = [NSNumber numberWithInt:1];
            break;
        case 18:
            Cls.witch = [NSNumber numberWithInt:1];
            break;
            
        default:
            break;
    }
    
    SkillData * Skills = [NSEntityDescription insertNewObjectForEntityForName:@"SkillData" inManagedObjectContext:context];
    
    Char.classes = Cls;
    Cls.character = Char;
    
    Char.skills = Skills;
    Skills.character = Char;
    
    [self saveContext];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"CharacterData" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    self.characters = [[context executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:[self.characters count] - 1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject * object = [self.characters objectAtIndex:indexPath.row];
        [[self context] deleteObject:object];
        
        [[self characters] removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self saveContext];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
