//
//  CharacterSpellTableViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/7/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "CharacterSpellTableViewController.h"
#import "SpellDetailsTableViewController.h"
#import "Helper.h"

@implementation CharacterSpellTableViewController {
    NSMutableArray * spellsAbjuration;
    NSMutableArray * spellsConjuration;
    NSMutableArray * spellsDivination;
    NSMutableArray * spellsEnchantment;
    NSMutableArray * spellsEvocation;
    NSMutableArray * spellsIllusion;
    NSMutableArray * spellsNecromancy;
    NSMutableArray * spellsTransmutation;
    NSMutableArray * spellsUniversal;
}

@synthesize parent;
@synthesize charIndex;
@synthesize spellList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSNumber * bardSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:1]];
    NSNumber * clericSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:2]];
    NSNumber * druidSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:3]];
    NSNumber * paladinSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:6]];
    NSNumber * rangerSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:7]];
    NSNumber * sorSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:9]];
    NSNumber * wizSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:10]];
    NSNumber * alchemistSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:11]];
    NSNumber * inquisitorSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:14]];
    NSNumber * magusSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:15]];
    NSNumber * oracleSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:16]];
    NSNumber * summonerSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:17]];
    NSNumber * witchSL = [Helper resolveClassLevelToSpellLevel:self.parent.classes class:[NSNumber numberWithInt:18]];
    if (bardSL.intValue == 999 && clericSL.intValue == 999 && druidSL.intValue == 999 && paladinSL.intValue == 999 && rangerSL.intValue == 999 && sorSL.intValue == 999 && wizSL.intValue == 999 && alchemistSL.intValue == 999 && inquisitorSL.intValue == 999 && magusSL.intValue == 999 && summonerSL.intValue == 999 && witchSL.intValue == 999) {
        spellsAbjuration = [[NSMutableArray alloc] init];
        spellsConjuration = [[NSMutableArray alloc] init];
        spellsDivination = [[NSMutableArray alloc] init];
        spellsEnchantment = [[NSMutableArray alloc] init];
        spellsEvocation = [[NSMutableArray alloc] init];
        spellsIllusion = [[NSMutableArray alloc] init];
        spellsNecromancy = [[NSMutableArray alloc] init];
        spellsTransmutation = [[NSMutableArray alloc] init];
        spellsUniversal = [[NSMutableArray alloc] init];
    } else {
        NSPredicate * bardSpell = [NSPredicate predicateWithFormat:@"bard <= %d", bardSL.intValue];
        NSPredicate * clericSpell = [NSPredicate predicateWithFormat:@"cleric <= %d", clericSL.intValue];
        NSPredicate * druidSpell = [NSPredicate predicateWithFormat:@"druid <= %d", druidSL.intValue];
        NSPredicate * paladinSpell = [NSPredicate predicateWithFormat:@"paladin <= %d", paladinSL.intValue];
        NSPredicate * rangerSpell = [NSPredicate predicateWithFormat:@"ranger <= %d", rangerSL.intValue];
        NSPredicate * sorSpell = [NSPredicate predicateWithFormat:@"sor <= %d", sorSL.intValue];
        NSPredicate * wizSpell = [NSPredicate predicateWithFormat:@"wiz <= %d", wizSL.intValue];
        NSPredicate * alchemistSpell = [NSPredicate predicateWithFormat:@"alchemist <= %d", alchemistSL.intValue];
        NSPredicate * inquisitorSpell = [NSPredicate predicateWithFormat:@"inquisitor <= %d", inquisitorSL.intValue];
        NSPredicate * magusSpell = [NSPredicate predicateWithFormat:@"magus <= %d", magusSL.intValue];
        NSPredicate * oracleSpell = [NSPredicate predicateWithFormat:@"oracle <= %d", oracleSL.intValue];
        NSPredicate * summonerSpell = [NSPredicate predicateWithFormat:@"summoner <= %d", summonerSL.intValue];
        NSPredicate * witchSpell = [NSPredicate predicateWithFormat:@"witch <= %d", witchSL.intValue];
        NSError * error;
        NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription * spellsEntity = [NSEntityDescription entityForName:@"SpellSource" inManagedObjectContext:self.parent.context];
        [fetchRequest setEntity:spellsEntity];
        NSSortDescriptor * sort = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:TRUE];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
        NSPredicate * abjPredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"abjuration"];
        NSPredicate * conPredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"conjuration"];
        NSPredicate * divPredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"divination"];
        NSPredicate * enchPredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"enchantment"];
        NSPredicate * evoPredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"evocation"];
        NSPredicate * illPredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"illusion"];
        NSPredicate * necroPredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"necromancy"];
        NSPredicate * transmutePredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"transmutation"];
        NSPredicate * uniPredicate = [NSPredicate predicateWithFormat:@"school like[c] %@", @"universal"];
        NSCompoundPredicate * abjCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:abjPredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:abjCompound];
        spellsAbjuration = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
        NSCompoundPredicate * conCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:conPredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:conCompound];
        spellsConjuration = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
        NSCompoundPredicate * divCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:divPredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:divCompound];
        spellsDivination = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
        NSCompoundPredicate * enchCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:enchPredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:enchCompound];
        spellsEnchantment = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
        NSCompoundPredicate * evoCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:evoPredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:evoCompound];
        spellsEvocation = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
        NSCompoundPredicate * illCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:illPredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:illCompound];
        spellsIllusion = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
        NSCompoundPredicate * necroCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:necroPredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:necroCompound];
        spellsNecromancy = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
        NSCompoundPredicate * transmuteCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:transmutePredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:transmuteCompound];
        spellsTransmutation = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
        NSCompoundPredicate * uniCompound = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:uniPredicate, wizSpell, bardSpell, clericSpell, druidSpell, paladinSpell, rangerSpell, sorSpell, alchemistSpell, inquisitorSpell, magusSpell, oracleSpell, summonerSpell, witchSpell, nil]];
        [fetchRequest setPredicate:uniCompound];
        spellsUniversal = [[self.parent.context executeFetchRequest:fetchRequest error:&error] mutableCopy];
    }
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SpellDetails"]) {
        SpellDetailsTableViewController * spellDetails = segue.destinationViewController;
        NSIndexPath * indexPath = [[self tableView] indexPathForSelectedRow];
        SpellSource * spell;
        
        switch (indexPath.section) {
            case 0:
                spell = [spellsAbjuration objectAtIndex:indexPath.row];
                break;
            case 1:
                spell = [spellsConjuration objectAtIndex:indexPath.row];
                break;
            case 2:
                spell = [spellsDivination objectAtIndex:indexPath.row];
                break;
            case 3:
                spell = [spellsEnchantment objectAtIndex:indexPath.row];
                break;
            case 4:
                spell = [spellsEvocation objectAtIndex:indexPath.row];
                break;
            case 5:
                spell = [spellsIllusion objectAtIndex:indexPath.row];
                break;
            case 6:
                spell = [spellsNecromancy objectAtIndex:indexPath.row];
                break;
            case 7:
                spell = [spellsTransmutation objectAtIndex:indexPath.row];
                break;
            case 8:
                spell = [spellsUniversal objectAtIndex:indexPath.row];
                break;
                
            default:
                spell = [spellsAbjuration objectAtIndex:indexPath.row];
                break;
        }
        spellDetails.spell = spell;
    }
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Abjuration";
        case 1:
            return @"Conjuration";
        case 2:
            return @"Divination";
        case 3:
            return @"Enchantment";
        case 4:
            return @"Evocation";
        case 5:
            return @"Illusion";
        case 6:
            return @"Necromancy";
        case 7:
            return @"Transmutation";
        case 8:
            return @"Universal";
            
        default:
            return @"Unknown";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 9;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [spellsAbjuration count];
        case 1:
            return [spellsConjuration count];
        case 2:
            return [spellsDivination count];
        case 3:
            return [spellsEnchantment count];
        case 4:
            return [spellsEvocation count];
        case 5:
            return [spellsIllusion count];
        case 6:
            return [spellsNecromancy count];
        case 7:
            return [spellsTransmutation count];
        case 8:
            return [spellsUniversal count];
            
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SpellCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    SpellSource * spell;
    
    switch (indexPath.section) {
        case 0:
            spell = [spellsAbjuration objectAtIndex:indexPath.row];
            break;
        case 1:
            spell = [spellsConjuration objectAtIndex:indexPath.row];
            break;
        case 2:
            spell = [spellsDivination objectAtIndex:indexPath.row];
            break;
        case 3:
            spell = [spellsEnchantment objectAtIndex:indexPath.row];
            break;
        case 4:
            spell = [spellsEvocation objectAtIndex:indexPath.row];
            break;
        case 5:
            spell = [spellsIllusion objectAtIndex:indexPath.row];
            break;
        case 6:
            spell = [spellsNecromancy objectAtIndex:indexPath.row];
            break;
        case 7:
            spell = [spellsTransmutation objectAtIndex:indexPath.row];
            break;
        case 8:
            spell = [spellsUniversal objectAtIndex:indexPath.row];
            break;
            
        default:
            spell = [spellsAbjuration objectAtIndex:indexPath.row];
            break;
    }
    cell.textLabel.text = spell.name;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
