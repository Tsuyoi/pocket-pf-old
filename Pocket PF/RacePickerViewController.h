//
//  RacePickerViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RacePickerViewController;

@protocol RacePickerViewControllerDelegate <NSObject>

- (void)racePickerViewController:(RacePickerViewController *)controller didSelectRace:(NSString *)raceName raceIndex:(int)index;

@end

@interface RacePickerViewController : UITableViewController

@property (weak, nonatomic) id <RacePickerViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString * race;

@end
