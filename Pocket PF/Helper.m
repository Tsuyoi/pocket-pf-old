//
//  Helper.m
//  Pocket PF
//
//  Created by Caylin Hickey on 3/27/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "Helper.h"

@implementation Helper

+ (NSString *)resolveAlignmentToString:(NSNumber *)index shortened: (BOOL)abbv{
    switch (index.intValue) {
        case 0:
            if (abbv) {return @"CG";} else {return @"Chaotic Good";}
        case 1:
            if (abbv) {return @"CN";} else {return @"Chaotic Neutral";}
        case 2:
            if (abbv) {return @"CE";} else {return @"Chaotic Evil";}
        case 3:
            if (abbv) {return @"NG";} else {return @"Neutral Good";}
        case 4:
            if (abbv) {return @"NN";} else {return @"True Neutral";}
        case 5:
            if (abbv) {return @"NE";} else {return @"Neutral Evil";}
        case 6:
            if (abbv) {return @"LG";} else {return @"Lawful Good";}
        case 7:
            if (abbv) {return @"LN";} else {return @"Lawful Neutral";}
        case 8:
            if (abbv) {return @"LE";} else {return @"Lawful Evil";}
            
        default:
            return @"Unknown";
    }
}

+ (NSNumber *)resolveBABFromClass:(ClassData *)index {
    int baseAttackBonus_1 = 0;
    int baseAttackBonus_2 = 0;
    int baseAttackBonus_3 = 0;
    int baseAttackBonus_4 = 0;
    switch (index.barbarian.intValue) {
        case 1:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 16;
            baseAttackBonus_2 += 11;
            baseAttackBonus_3 += 6;
            baseAttackBonus_4 += 1;
            break;
        case 17:
            baseAttackBonus_1 += 17;
            baseAttackBonus_2 += 12;
            baseAttackBonus_3 += 7;
            baseAttackBonus_4 += 2;
            break;
        case 18:
            baseAttackBonus_1 += 18;
            baseAttackBonus_2 += 13;
            baseAttackBonus_3 += 8;
            baseAttackBonus_4 += 3;
            break;
        case 19:
            baseAttackBonus_1 += 19;
            baseAttackBonus_2 += 14;
            baseAttackBonus_3 += 9;
            baseAttackBonus_4 += 4;
            break;
        case 20:
            baseAttackBonus_1 += 20;
            baseAttackBonus_2 += 15;
            baseAttackBonus_3 += 10;
            baseAttackBonus_4 += 5;
            break;
            
        default:
            break;
    } // 1 2 3 4 5
    switch (index.bard.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.cleric.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.druid.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.fighter.intValue) {
        case 1:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 16;
            baseAttackBonus_2 += 11;
            baseAttackBonus_3 += 6;
            baseAttackBonus_4 += 1;
            break;
        case 17:
            baseAttackBonus_1 += 17;
            baseAttackBonus_2 += 12;
            baseAttackBonus_3 += 7;
            baseAttackBonus_4 += 2;
            break;
        case 18:
            baseAttackBonus_1 += 18;
            baseAttackBonus_2 += 13;
            baseAttackBonus_3 += 8;
            baseAttackBonus_4 += 3;
            break;
        case 19:
            baseAttackBonus_1 += 19;
            baseAttackBonus_2 += 14;
            baseAttackBonus_3 += 9;
            baseAttackBonus_4 += 4;
            break;
        case 20:
            baseAttackBonus_1 += 20;
            baseAttackBonus_2 += 15;
            baseAttackBonus_3 += 10;
            baseAttackBonus_4 += 5;
            break;
            
        default:
            break;
    } // 1 2 3 4 5
    switch (index.monk.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.paladin.intValue) {
        case 1:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 16;
            baseAttackBonus_2 += 11;
            baseAttackBonus_3 += 6;
            baseAttackBonus_4 += 1;
            break;
        case 17:
            baseAttackBonus_1 += 17;
            baseAttackBonus_2 += 12;
            baseAttackBonus_3 += 7;
            baseAttackBonus_4 += 2;
            break;
        case 18:
            baseAttackBonus_1 += 18;
            baseAttackBonus_2 += 13;
            baseAttackBonus_3 += 8;
            baseAttackBonus_4 += 3;
            break;
        case 19:
            baseAttackBonus_1 += 19;
            baseAttackBonus_2 += 14;
            baseAttackBonus_3 += 9;
            baseAttackBonus_4 += 4;
            break;
        case 20:
            baseAttackBonus_1 += 20;
            baseAttackBonus_2 += 15;
            baseAttackBonus_3 += 10;
            baseAttackBonus_4 += 5;
            break;
            
        default:
            break;
    } // 1 2 3 4 5
    switch (index.ranger.intValue) {
        case 1:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 16;
            baseAttackBonus_2 += 11;
            baseAttackBonus_3 += 6;
            baseAttackBonus_4 += 1;
            break;
        case 17:
            baseAttackBonus_1 += 17;
            baseAttackBonus_2 += 12;
            baseAttackBonus_3 += 7;
            baseAttackBonus_4 += 2;
            break;
        case 18:
            baseAttackBonus_1 += 18;
            baseAttackBonus_2 += 13;
            baseAttackBonus_3 += 8;
            baseAttackBonus_4 += 3;
            break;
        case 19:
            baseAttackBonus_1 += 19;
            baseAttackBonus_2 += 14;
            baseAttackBonus_3 += 9;
            baseAttackBonus_4 += 4;
            break;
        case 20:
            baseAttackBonus_1 += 20;
            baseAttackBonus_2 += 15;
            baseAttackBonus_3 += 10;
            baseAttackBonus_4 += 5;
            break;
            
        default:
            break;
    } // 1 2 3 4 5
    switch (index.rogue.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.sorcerer.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 1 2 2
    switch (index.wizard.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 1 2 2
    switch (index.alchemist.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.cavalier.intValue) {
        case 1:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 16;
            baseAttackBonus_2 += 11;
            baseAttackBonus_3 += 6;
            baseAttackBonus_4 += 1;
            break;
        case 17:
            baseAttackBonus_1 += 17;
            baseAttackBonus_2 += 12;
            baseAttackBonus_3 += 7;
            baseAttackBonus_4 += 2;
            break;
        case 18:
            baseAttackBonus_1 += 18;
            baseAttackBonus_2 += 13;
            baseAttackBonus_3 += 8;
            baseAttackBonus_4 += 3;
            break;
        case 19:
            baseAttackBonus_1 += 19;
            baseAttackBonus_2 += 14;
            baseAttackBonus_3 += 9;
            baseAttackBonus_4 += 4;
            break;
        case 20:
            baseAttackBonus_1 += 20;
            baseAttackBonus_2 += 15;
            baseAttackBonus_3 += 10;
            baseAttackBonus_4 += 5;
            break;
            
        default:
            break;
    } // 1 2 3 4 5
    switch (index.gunslinger.intValue) {
        case 1:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 16;
            baseAttackBonus_2 += 11;
            baseAttackBonus_3 += 6;
            baseAttackBonus_4 += 1;
            break;
        case 17:
            baseAttackBonus_1 += 17;
            baseAttackBonus_2 += 12;
            baseAttackBonus_3 += 7;
            baseAttackBonus_4 += 2;
            break;
        case 18:
            baseAttackBonus_1 += 18;
            baseAttackBonus_2 += 13;
            baseAttackBonus_3 += 8;
            baseAttackBonus_4 += 3;
            break;
        case 19:
            baseAttackBonus_1 += 19;
            baseAttackBonus_2 += 14;
            baseAttackBonus_3 += 9;
            baseAttackBonus_4 += 4;
            break;
        case 20:
            baseAttackBonus_1 += 20;
            baseAttackBonus_2 += 15;
            baseAttackBonus_3 += 10;
            baseAttackBonus_4 += 5;
            break;
            
        default:
            break;
    } // 1 2 3 4 5
    switch (index.inquisitor.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.magus.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.oracle.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.summoner.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 11;
            baseAttackBonus_2 += 6;
            baseAttackBonus_3 += 1;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 7;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 12;
            baseAttackBonus_2 += 17;
            baseAttackBonus_3 += 2;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 13;
            baseAttackBonus_2 += 8;
            baseAttackBonus_3 += 3;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 14;
            baseAttackBonus_2 += 9;
            baseAttackBonus_3 += 4;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 15;
            baseAttackBonus_2 += 10;
            baseAttackBonus_3 += 5;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 2 3 3
    switch (index.witch.intValue) {
        case 1:
            baseAttackBonus_1 += 0;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 2:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 3:
            baseAttackBonus_1 += 1;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 4:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 5:
            baseAttackBonus_1 += 2;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 6:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 7:
            baseAttackBonus_1 += 3;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 8:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 9:
            baseAttackBonus_1 += 4;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 10:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 11:
            baseAttackBonus_1 += 5;
            baseAttackBonus_2 += 0;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 12:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 13:
            baseAttackBonus_1 += 6;
            baseAttackBonus_2 += 1;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 14:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 15:
            baseAttackBonus_1 += 7;
            baseAttackBonus_2 += 2;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 16:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 17:
            baseAttackBonus_1 += 8;
            baseAttackBonus_2 += 3;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 18:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 19:
            baseAttackBonus_1 += 9;
            baseAttackBonus_2 += 4;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
        case 20:
            baseAttackBonus_1 += 10;
            baseAttackBonus_2 += 5;
            baseAttackBonus_3 += 0;
            baseAttackBonus_4 += 0;
            break;
            
        default:
            break;
    } // 0 1 1 2 2
    return [NSNumber numberWithInt:baseAttackBonus_1];
}

+ (NSNumber *)resolveFortSaveFromClass:(ClassData *)index {
    int fortSave = 0;
    switch (index.barbarian.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.bard.intValue) {
        case 1:
            fortSave += 0;
            break;
        case 2:
            fortSave += 0;
            break;
        case 3:
            fortSave += 1;
            break;
        case 4:
            fortSave += 1;
            break;
        case 5:
            fortSave += 1;
            break;
        case 6:
            fortSave += 2;
            break;
        case 7:
            fortSave += 2;
            break;
        case 8:
            fortSave += 2;
            break;
        case 9:
            fortSave += 3;
            break;
        case 10:
            fortSave += 3;
            break;
        case 11:
            fortSave += 3;
            break;
        case 12:
            fortSave += 4;
            break;
        case 13:
            fortSave += 4;
            break;
        case 14:
            fortSave += 4;
            break;
        case 15:
            fortSave += 5;
            break;
        case 16:
            fortSave += 5;
            break;
        case 17:
            fortSave += 5;
            break;
        case 18:
            fortSave += 6;
            break;
        case 19:
            fortSave += 6;
            break;
        case 20:
            fortSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.cleric.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.druid.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.fighter.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.monk.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.paladin.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.ranger.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.rogue.intValue) {
        case 1:
            fortSave += 0;
            break;
        case 2:
            fortSave += 0;
            break;
        case 3:
            fortSave += 1;
            break;
        case 4:
            fortSave += 1;
            break;
        case 5:
            fortSave += 1;
            break;
        case 6:
            fortSave += 2;
            break;
        case 7:
            fortSave += 2;
            break;
        case 8:
            fortSave += 2;
            break;
        case 9:
            fortSave += 3;
            break;
        case 10:
            fortSave += 3;
            break;
        case 11:
            fortSave += 3;
            break;
        case 12:
            fortSave += 4;
            break;
        case 13:
            fortSave += 4;
            break;
        case 14:
            fortSave += 4;
            break;
        case 15:
            fortSave += 5;
            break;
        case 16:
            fortSave += 5;
            break;
        case 17:
            fortSave += 5;
            break;
        case 18:
            fortSave += 6;
            break;
        case 19:
            fortSave += 6;
            break;
        case 20:
            fortSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.sorcerer.intValue) {
        case 1:
            fortSave += 0;
            break;
        case 2:
            fortSave += 0;
            break;
        case 3:
            fortSave += 1;
            break;
        case 4:
            fortSave += 1;
            break;
        case 5:
            fortSave += 1;
            break;
        case 6:
            fortSave += 2;
            break;
        case 7:
            fortSave += 2;
            break;
        case 8:
            fortSave += 2;
            break;
        case 9:
            fortSave += 3;
            break;
        case 10:
            fortSave += 3;
            break;
        case 11:
            fortSave += 3;
            break;
        case 12:
            fortSave += 4;
            break;
        case 13:
            fortSave += 4;
            break;
        case 14:
            fortSave += 4;
            break;
        case 15:
            fortSave += 5;
            break;
        case 16:
            fortSave += 5;
            break;
        case 17:
            fortSave += 5;
            break;
        case 18:
            fortSave += 6;
            break;
        case 19:
            fortSave += 6;
            break;
        case 20:
            fortSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.wizard.intValue) {
        case 1:
            fortSave += 0;
            break;
        case 2:
            fortSave += 0;
            break;
        case 3:
            fortSave += 1;
            break;
        case 4:
            fortSave += 1;
            break;
        case 5:
            fortSave += 1;
            break;
        case 6:
            fortSave += 2;
            break;
        case 7:
            fortSave += 2;
            break;
        case 8:
            fortSave += 2;
            break;
        case 9:
            fortSave += 3;
            break;
        case 10:
            fortSave += 3;
            break;
        case 11:
            fortSave += 3;
            break;
        case 12:
            fortSave += 4;
            break;
        case 13:
            fortSave += 4;
            break;
        case 14:
            fortSave += 4;
            break;
        case 15:
            fortSave += 5;
            break;
        case 16:
            fortSave += 5;
            break;
        case 17:
            fortSave += 5;
            break;
        case 18:
            fortSave += 6;
            break;
        case 19:
            fortSave += 6;
            break;
        case 20:
            fortSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.alchemist.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.cavalier.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.gunslinger.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.inquisitor.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.magus.intValue) {
        case 1:
            fortSave += 2;
            break;
        case 2:
            fortSave += 3;
            break;
        case 3:
            fortSave += 3;
            break;
        case 4:
            fortSave += 4;
            break;
        case 5:
            fortSave += 4;
            break;
        case 6:
            fortSave += 5;
            break;
        case 7:
            fortSave += 5;
            break;
        case 8:
            fortSave += 6;
            break;
        case 9:
            fortSave += 6;
            break;
        case 10:
            fortSave += 7;
            break;
        case 11:
            fortSave += 7;
            break;
        case 12:
            fortSave += 8;
            break;
        case 13:
            fortSave += 8;
            break;
        case 14:
            fortSave += 9;
            break;
        case 15:
            fortSave += 9;
            break;
        case 16:
            fortSave += 10;
            break;
        case 17:
            fortSave += 10;
            break;
        case 18:
            fortSave += 11;
            break;
        case 19:
            fortSave += 11;
            break;
        case 20:
            fortSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.oracle.intValue) {
        case 1:
            fortSave += 0;
            break;
        case 2:
            fortSave += 0;
            break;
        case 3:
            fortSave += 1;
            break;
        case 4:
            fortSave += 1;
            break;
        case 5:
            fortSave += 1;
            break;
        case 6:
            fortSave += 2;
            break;
        case 7:
            fortSave += 2;
            break;
        case 8:
            fortSave += 2;
            break;
        case 9:
            fortSave += 3;
            break;
        case 10:
            fortSave += 3;
            break;
        case 11:
            fortSave += 3;
            break;
        case 12:
            fortSave += 4;
            break;
        case 13:
            fortSave += 4;
            break;
        case 14:
            fortSave += 4;
            break;
        case 15:
            fortSave += 5;
            break;
        case 16:
            fortSave += 5;
            break;
        case 17:
            fortSave += 5;
            break;
        case 18:
            fortSave += 6;
            break;
        case 19:
            fortSave += 6;
            break;
        case 20:
            fortSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.summoner.intValue) {
        case 1:
            fortSave += 0;
            break;
        case 2:
            fortSave += 0;
            break;
        case 3:
            fortSave += 1;
            break;
        case 4:
            fortSave += 1;
            break;
        case 5:
            fortSave += 1;
            break;
        case 6:
            fortSave += 2;
            break;
        case 7:
            fortSave += 2;
            break;
        case 8:
            fortSave += 2;
            break;
        case 9:
            fortSave += 3;
            break;
        case 10:
            fortSave += 3;
            break;
        case 11:
            fortSave += 3;
            break;
        case 12:
            fortSave += 4;
            break;
        case 13:
            fortSave += 4;
            break;
        case 14:
            fortSave += 4;
            break;
        case 15:
            fortSave += 5;
            break;
        case 16:
            fortSave += 5;
            break;
        case 17:
            fortSave += 5;
            break;
        case 18:
            fortSave += 6;
            break;
        case 19:
            fortSave += 6;
            break;
        case 20:
            fortSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.witch.intValue) {
        case 1:
            fortSave += 0;
            break;
        case 2:
            fortSave += 0;
            break;
        case 3:
            fortSave += 1;
            break;
        case 4:
            fortSave += 1;
            break;
        case 5:
            fortSave += 1;
            break;
        case 6:
            fortSave += 2;
            break;
        case 7:
            fortSave += 2;
            break;
        case 8:
            fortSave += 2;
            break;
        case 9:
            fortSave += 3;
            break;
        case 10:
            fortSave += 3;
            break;
        case 11:
            fortSave += 3;
            break;
        case 12:
            fortSave += 4;
            break;
        case 13:
            fortSave += 4;
            break;
        case 14:
            fortSave += 4;
            break;
        case 15:
            fortSave += 5;
            break;
        case 16:
            fortSave += 5;
            break;
        case 17:
            fortSave += 5;
            break;
        case 18:
            fortSave += 6;
            break;
        case 19:
            fortSave += 6;
            break;
        case 20:
            fortSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    return [NSNumber numberWithInt:fortSave];
}

+ (NSNumber *)resolveRefSaveFromClass:(ClassData *)index {
    int refSave = 0;
    switch (index.barbarian.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.bard.intValue) {
        case 1:
            refSave += 2;
            break;
        case 2:
            refSave += 3;
            break;
        case 3:
            refSave += 3;
            break;
        case 4:
            refSave += 4;
            break;
        case 5:
            refSave += 4;
            break;
        case 6:
            refSave += 5;
            break;
        case 7:
            refSave += 5;
            break;
        case 8:
            refSave += 6;
            break;
        case 9:
            refSave += 6;
            break;
        case 10:
            refSave += 7;
            break;
        case 11:
            refSave += 7;
            break;
        case 12:
            refSave += 8;
            break;
        case 13:
            refSave += 8;
            break;
        case 14:
            refSave += 9;
            break;
        case 15:
            refSave += 9;
            break;
        case 16:
            refSave += 10;
            break;
        case 17:
            refSave += 10;
            break;
        case 18:
            refSave += 11;
            break;
        case 19:
            refSave += 11;
            break;
        case 20:
            refSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.cleric.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.druid.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.fighter.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.monk.intValue) {
        case 1:
            refSave += 2;
            break;
        case 2:
            refSave += 3;
            break;
        case 3:
            refSave += 3;
            break;
        case 4:
            refSave += 4;
            break;
        case 5:
            refSave += 4;
            break;
        case 6:
            refSave += 5;
            break;
        case 7:
            refSave += 5;
            break;
        case 8:
            refSave += 6;
            break;
        case 9:
            refSave += 6;
            break;
        case 10:
            refSave += 7;
            break;
        case 11:
            refSave += 7;
            break;
        case 12:
            refSave += 8;
            break;
        case 13:
            refSave += 8;
            break;
        case 14:
            refSave += 9;
            break;
        case 15:
            refSave += 9;
            break;
        case 16:
            refSave += 10;
            break;
        case 17:
            refSave += 10;
            break;
        case 18:
            refSave += 11;
            break;
        case 19:
            refSave += 11;
            break;
        case 20:
            refSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.paladin.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.ranger.intValue) {
        case 1:
            refSave += 2;
            break;
        case 2:
            refSave += 3;
            break;
        case 3:
            refSave += 3;
            break;
        case 4:
            refSave += 4;
            break;
        case 5:
            refSave += 4;
            break;
        case 6:
            refSave += 5;
            break;
        case 7:
            refSave += 5;
            break;
        case 8:
            refSave += 6;
            break;
        case 9:
            refSave += 6;
            break;
        case 10:
            refSave += 7;
            break;
        case 11:
            refSave += 7;
            break;
        case 12:
            refSave += 8;
            break;
        case 13:
            refSave += 8;
            break;
        case 14:
            refSave += 9;
            break;
        case 15:
            refSave += 9;
            break;
        case 16:
            refSave += 10;
            break;
        case 17:
            refSave += 10;
            break;
        case 18:
            refSave += 11;
            break;
        case 19:
            refSave += 11;
            break;
        case 20:
            refSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.rogue.intValue) {
        case 1:
            refSave += 2;
            break;
        case 2:
            refSave += 3;
            break;
        case 3:
            refSave += 3;
            break;
        case 4:
            refSave += 4;
            break;
        case 5:
            refSave += 4;
            break;
        case 6:
            refSave += 5;
            break;
        case 7:
            refSave += 5;
            break;
        case 8:
            refSave += 6;
            break;
        case 9:
            refSave += 6;
            break;
        case 10:
            refSave += 7;
            break;
        case 11:
            refSave += 7;
            break;
        case 12:
            refSave += 8;
            break;
        case 13:
            refSave += 8;
            break;
        case 14:
            refSave += 9;
            break;
        case 15:
            refSave += 9;
            break;
        case 16:
            refSave += 10;
            break;
        case 17:
            refSave += 10;
            break;
        case 18:
            refSave += 11;
            break;
        case 19:
            refSave += 11;
            break;
        case 20:
            refSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.sorcerer.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.wizard.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.alchemist.intValue) {
        case 1:
            refSave += 2;
            break;
        case 2:
            refSave += 3;
            break;
        case 3:
            refSave += 3;
            break;
        case 4:
            refSave += 4;
            break;
        case 5:
            refSave += 4;
            break;
        case 6:
            refSave += 5;
            break;
        case 7:
            refSave += 5;
            break;
        case 8:
            refSave += 6;
            break;
        case 9:
            refSave += 6;
            break;
        case 10:
            refSave += 7;
            break;
        case 11:
            refSave += 7;
            break;
        case 12:
            refSave += 8;
            break;
        case 13:
            refSave += 8;
            break;
        case 14:
            refSave += 9;
            break;
        case 15:
            refSave += 9;
            break;
        case 16:
            refSave += 10;
            break;
        case 17:
            refSave += 10;
            break;
        case 18:
            refSave += 11;
            break;
        case 19:
            refSave += 11;
            break;
        case 20:
            refSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.cavalier.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.gunslinger.intValue) {
        case 1:
            refSave += 2;
            break;
        case 2:
            refSave += 3;
            break;
        case 3:
            refSave += 3;
            break;
        case 4:
            refSave += 4;
            break;
        case 5:
            refSave += 4;
            break;
        case 6:
            refSave += 5;
            break;
        case 7:
            refSave += 5;
            break;
        case 8:
            refSave += 6;
            break;
        case 9:
            refSave += 6;
            break;
        case 10:
            refSave += 7;
            break;
        case 11:
            refSave += 7;
            break;
        case 12:
            refSave += 8;
            break;
        case 13:
            refSave += 8;
            break;
        case 14:
            refSave += 9;
            break;
        case 15:
            refSave += 9;
            break;
        case 16:
            refSave += 10;
            break;
        case 17:
            refSave += 10;
            break;
        case 18:
            refSave += 11;
            break;
        case 19:
            refSave += 11;
            break;
        case 20:
            refSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.inquisitor.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.magus.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.oracle.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.summoner.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.witch.intValue) {
        case 1:
            refSave += 0;
            break;
        case 2:
            refSave += 0;
            break;
        case 3:
            refSave += 1;
            break;
        case 4:
            refSave += 1;
            break;
        case 5:
            refSave += 1;
            break;
        case 6:
            refSave += 2;
            break;
        case 7:
            refSave += 2;
            break;
        case 8:
            refSave += 2;
            break;
        case 9:
            refSave += 3;
            break;
        case 10:
            refSave += 3;
            break;
        case 11:
            refSave += 3;
            break;
        case 12:
            refSave += 4;
            break;
        case 13:
            refSave += 4;
            break;
        case 14:
            refSave += 4;
            break;
        case 15:
            refSave += 5;
            break;
        case 16:
            refSave += 5;
            break;
        case 17:
            refSave += 5;
            break;
        case 18:
            refSave += 6;
            break;
        case 19:
            refSave += 6;
            break;
        case 20:
            refSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    return [NSNumber numberWithInt:refSave];
}

+ (NSNumber *)resolveWillSaveFromClass:(ClassData *)index {
    int willSave = 0;
    switch (index.barbarian.intValue) {
        case 1:
            willSave += 0;
            break;
        case 2:
            willSave += 0;
            break;
        case 3:
            willSave += 1;
            break;
        case 4:
            willSave += 1;
            break;
        case 5:
            willSave += 1;
            break;
        case 6:
            willSave += 2;
            break;
        case 7:
            willSave += 2;
            break;
        case 8:
            willSave += 2;
            break;
        case 9:
            willSave += 3;
            break;
        case 10:
            willSave += 3;
            break;
        case 11:
            willSave += 3;
            break;
        case 12:
            willSave += 4;
            break;
        case 13:
            willSave += 4;
            break;
        case 14:
            willSave += 4;
            break;
        case 15:
            willSave += 5;
            break;
        case 16:
            willSave += 5;
            break;
        case 17:
            willSave += 5;
            break;
        case 18:
            willSave += 6;
            break;
        case 19:
            willSave += 6;
            break;
        case 20:
            willSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.bard.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.cleric.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.druid.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.fighter.intValue) {
        case 1:
            willSave += 0;
            break;
        case 2:
            willSave += 0;
            break;
        case 3:
            willSave += 1;
            break;
        case 4:
            willSave += 1;
            break;
        case 5:
            willSave += 1;
            break;
        case 6:
            willSave += 2;
            break;
        case 7:
            willSave += 2;
            break;
        case 8:
            willSave += 2;
            break;
        case 9:
            willSave += 3;
            break;
        case 10:
            willSave += 3;
            break;
        case 11:
            willSave += 3;
            break;
        case 12:
            willSave += 4;
            break;
        case 13:
            willSave += 4;
            break;
        case 14:
            willSave += 4;
            break;
        case 15:
            willSave += 5;
            break;
        case 16:
            willSave += 5;
            break;
        case 17:
            willSave += 5;
            break;
        case 18:
            willSave += 6;
            break;
        case 19:
            willSave += 6;
            break;
        case 20:
            willSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.monk.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.paladin.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.ranger.intValue) {
        case 1:
            willSave += 0;
            break;
        case 2:
            willSave += 0;
            break;
        case 3:
            willSave += 1;
            break;
        case 4:
            willSave += 1;
            break;
        case 5:
            willSave += 1;
            break;
        case 6:
            willSave += 2;
            break;
        case 7:
            willSave += 2;
            break;
        case 8:
            willSave += 2;
            break;
        case 9:
            willSave += 3;
            break;
        case 10:
            willSave += 3;
            break;
        case 11:
            willSave += 3;
            break;
        case 12:
            willSave += 4;
            break;
        case 13:
            willSave += 4;
            break;
        case 14:
            willSave += 4;
            break;
        case 15:
            willSave += 5;
            break;
        case 16:
            willSave += 5;
            break;
        case 17:
            willSave += 5;
            break;
        case 18:
            willSave += 6;
            break;
        case 19:
            willSave += 6;
            break;
        case 20:
            willSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.rogue.intValue) {
        case 1:
            willSave += 0;
            break;
        case 2:
            willSave += 0;
            break;
        case 3:
            willSave += 1;
            break;
        case 4:
            willSave += 1;
            break;
        case 5:
            willSave += 1;
            break;
        case 6:
            willSave += 2;
            break;
        case 7:
            willSave += 2;
            break;
        case 8:
            willSave += 2;
            break;
        case 9:
            willSave += 3;
            break;
        case 10:
            willSave += 3;
            break;
        case 11:
            willSave += 3;
            break;
        case 12:
            willSave += 4;
            break;
        case 13:
            willSave += 4;
            break;
        case 14:
            willSave += 4;
            break;
        case 15:
            willSave += 5;
            break;
        case 16:
            willSave += 5;
            break;
        case 17:
            willSave += 5;
            break;
        case 18:
            willSave += 6;
            break;
        case 19:
            willSave += 6;
            break;
        case 20:
            willSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.sorcerer.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.wizard.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.alchemist.intValue) {
        case 1:
            willSave += 0;
            break;
        case 2:
            willSave += 0;
            break;
        case 3:
            willSave += 1;
            break;
        case 4:
            willSave += 1;
            break;
        case 5:
            willSave += 1;
            break;
        case 6:
            willSave += 2;
            break;
        case 7:
            willSave += 2;
            break;
        case 8:
            willSave += 2;
            break;
        case 9:
            willSave += 3;
            break;
        case 10:
            willSave += 3;
            break;
        case 11:
            willSave += 3;
            break;
        case 12:
            willSave += 4;
            break;
        case 13:
            willSave += 4;
            break;
        case 14:
            willSave += 4;
            break;
        case 15:
            willSave += 5;
            break;
        case 16:
            willSave += 5;
            break;
        case 17:
            willSave += 5;
            break;
        case 18:
            willSave += 6;
            break;
        case 19:
            willSave += 6;
            break;
        case 20:
            willSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.cavalier.intValue) {
        case 1:
            willSave += 0;
            break;
        case 2:
            willSave += 0;
            break;
        case 3:
            willSave += 1;
            break;
        case 4:
            willSave += 1;
            break;
        case 5:
            willSave += 1;
            break;
        case 6:
            willSave += 2;
            break;
        case 7:
            willSave += 2;
            break;
        case 8:
            willSave += 2;
            break;
        case 9:
            willSave += 3;
            break;
        case 10:
            willSave += 3;
            break;
        case 11:
            willSave += 3;
            break;
        case 12:
            willSave += 4;
            break;
        case 13:
            willSave += 4;
            break;
        case 14:
            willSave += 4;
            break;
        case 15:
            willSave += 5;
            break;
        case 16:
            willSave += 5;
            break;
        case 17:
            willSave += 5;
            break;
        case 18:
            willSave += 6;
            break;
        case 19:
            willSave += 6;
            break;
        case 20:
            willSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.gunslinger.intValue) {
        case 1:
            willSave += 0;
            break;
        case 2:
            willSave += 0;
            break;
        case 3:
            willSave += 1;
            break;
        case 4:
            willSave += 1;
            break;
        case 5:
            willSave += 1;
            break;
        case 6:
            willSave += 2;
            break;
        case 7:
            willSave += 2;
            break;
        case 8:
            willSave += 2;
            break;
        case 9:
            willSave += 3;
            break;
        case 10:
            willSave += 3;
            break;
        case 11:
            willSave += 3;
            break;
        case 12:
            willSave += 4;
            break;
        case 13:
            willSave += 4;
            break;
        case 14:
            willSave += 4;
            break;
        case 15:
            willSave += 5;
            break;
        case 16:
            willSave += 5;
            break;
        case 17:
            willSave += 5;
            break;
        case 18:
            willSave += 6;
            break;
        case 19:
            willSave += 6;
            break;
        case 20:
            willSave += 6;
            break;
            
        default:
            break;
    } // 0 0 1 1 1
    switch (index.inquisitor.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.magus.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.oracle.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.summoner.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    switch (index.witch.intValue) {
        case 1:
            willSave += 2;
            break;
        case 2:
            willSave += 3;
            break;
        case 3:
            willSave += 3;
            break;
        case 4:
            willSave += 4;
            break;
        case 5:
            willSave += 4;
            break;
        case 6:
            willSave += 5;
            break;
        case 7:
            willSave += 5;
            break;
        case 8:
            willSave += 6;
            break;
        case 9:
            willSave += 6;
            break;
        case 10:
            willSave += 7;
            break;
        case 11:
            willSave += 7;
            break;
        case 12:
            willSave += 8;
            break;
        case 13:
            willSave += 8;
            break;
        case 14:
            willSave += 9;
            break;
        case 15:
            willSave += 9;
            break;
        case 16:
            willSave += 10;
            break;
        case 17:
            willSave += 10;
            break;
        case 18:
            willSave += 11;
            break;
        case 19:
            willSave += 11;
            break;
        case 20:
            willSave += 12;
            break;
            
        default:
            break;
    } // 2 3 3 4 4
    return [NSNumber numberWithInt:willSave];
}

+ (NSString *)resolveClassToString:(NSNumber *)index {
    switch (index.intValue) {
        case 0:
            return @"Barbarian";
        case 1:
            return @"Bard";
        case 2:
            return @"Cleric";
        case 3:
            return @"Druid";
        case 4:
            return @"Fighter";
        case 5:
            return @"Monk";
        case 6:
            return @"Paladin";
        case 7:
            return @"Ranger";
        case 8:
            return @"Rogue";
        case 9:
            return @"Sorcerer";
        case 10:
            return @"Wizard";
        case 11:
            return @"Alchemist";
        case 12:
            return @"Cavalier";
        case 13:
            return @"Gunslinger";
        case 14:
            return @"Inquisitor";
        case 15:
            return @"Magus";
        case 16:
            return @"Oracle";
        case 17:
            return @"Summoner";
        case 18:
            return @"Witch";
            
        default:
            return @"Unknown";
    }
}

+ (NSNumber *)resolveClassLevelToSpellLevel:(ClassData *)index class:(NSNumber *)classNumber {
    switch (classNumber.intValue) {
        case 1: // Bard
            switch (index.bard.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:1];
                case 4:
                case 5:
                case 6:
                    return [NSNumber numberWithInt:2];
                case 7:
                case 8:
                case 9:
                    return [NSNumber numberWithInt:3];
                case 10:
                case 11:
                case 12:
                    return [NSNumber numberWithInt:4];
                case 13:
                case 14:
                case 15:
                    return [NSNumber numberWithInt:5];
                    
                default:
                    return [NSNumber numberWithInt:6];
            }
            break;
        case 2: // Cleric
            switch (index.cleric.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                    break;
                case 1:
                case 2:
                    return [NSNumber numberWithInt:1];
                case 3:
                case 4:
                    return [NSNumber numberWithInt:2];
                case 5:
                case 6:
                    return [NSNumber numberWithInt:3];
                case 7:
                case 8:
                    return [NSNumber numberWithInt:4];
                case 9:
                case 10:
                    return [NSNumber numberWithInt:5];
                case 11:
                case 12:
                    return [NSNumber numberWithInt:6];
                case 13:
                case 14:
                    return [NSNumber numberWithInt:7];
                case 15:
                case 16:
                    return [NSNumber numberWithInt:8];
                    
                default:
                    return [NSNumber numberWithInt:9];
            }
            break;
        case 3: // Druid
            switch (index.druid.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                    break;
                case 1:
                case 2:
                    return [NSNumber numberWithInt:1];
                case 3:
                case 4:
                    return [NSNumber numberWithInt:2];
                case 5:
                case 6:
                    return [NSNumber numberWithInt:3];
                case 7:
                case 8:
                    return [NSNumber numberWithInt:4];
                case 9:
                case 10:
                    return [NSNumber numberWithInt:5];
                case 11:
                case 12:
                    return [NSNumber numberWithInt:6];
                case 13:
                case 14:
                    return [NSNumber numberWithInt:7];
                case 15:
                case 16:
                    return [NSNumber numberWithInt:8];
                    
                default:
                    return [NSNumber numberWithInt:9];
            }
            break;
        case 6: // Paladin
            switch (index.paladin.intValue) {
                case 0:
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:999];
                case 4:
                case 5:
                case 6:
                    return [NSNumber numberWithInt:1];
                case 7:
                case 8:
                case 9:
                    
                    return [NSNumber numberWithInt:2];
                case 10:
                case 11:
                case 12:
                    
                    return [NSNumber numberWithInt:3];
                default:
                    return [NSNumber numberWithInt:4];
            }
            break;
        case 7: // Ranger
            switch (index.ranger.intValue) {
                case 0:
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:999];
                case 4:
                case 5:
                case 6:
                    return [NSNumber numberWithInt:1];
                case 7:
                case 8:
                case 9:
                    
                    return [NSNumber numberWithInt:2];
                case 10:
                case 11:
                case 12:
                    
                    return [NSNumber numberWithInt:3];
                default:
                    return [NSNumber numberWithInt:4];
            }
            break;
        case 9: // Sorcerer
            switch (index.sorcerer.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:1];
                case 4:
                case 5:
                    return [NSNumber numberWithInt:2];
                case 6:
                case 7:
                    return [NSNumber numberWithInt:3];
                case 8:
                case 9:
                    return [NSNumber numberWithInt:4];
                case 10:
                case 11:
                    return [NSNumber numberWithInt:5];
                case 12:
                case 13:
                    return [NSNumber numberWithInt:6];
                case 14:
                case 15:
                    return [NSNumber numberWithInt:7];
                case 16:
                case 17:
                    return [NSNumber numberWithInt:8];
                    
                default:
                    return [NSNumber numberWithInt:9];
            }
            break;
        case 10: // Wizard
            switch (index.wizard.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                    break;
                case 1:
                case 2:
                    return [NSNumber numberWithInt:1];
                case 3:
                case 4:
                    return [NSNumber numberWithInt:2];
                case 5:
                case 6:
                    return [NSNumber numberWithInt:3];
                case 7:
                case 8:
                    return [NSNumber numberWithInt:4];
                case 9:
                case 10:
                    return [NSNumber numberWithInt:5];
                case 11:
                case 12:
                    return [NSNumber numberWithInt:6];
                case 13:
                case 14:
                    return [NSNumber numberWithInt:7];
                case 15:
                case 16:
                    return [NSNumber numberWithInt:8];
                    
                default:
                    return [NSNumber numberWithInt:9];
            }
            break;
        case 11: // Alchemist
            switch (index.alchemist.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:1];
                case 4:
                case 5:
                case 6:
                    return [NSNumber numberWithInt:2];
                case 7:
                case 8:
                case 9:
                    return [NSNumber numberWithInt:3];
                case 10:
                case 11:
                case 12:
                    return [NSNumber numberWithInt:4];
                case 13:
                case 14:
                case 15:
                    return [NSNumber numberWithInt:5];
                    
                default:
                    return [NSNumber numberWithInt:6];
            }
            break;
        case 14: // Inquisitor
            switch (index.inquisitor.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:1];
                case 4:
                case 5:
                case 6:
                    return [NSNumber numberWithInt:2];
                case 7:
                case 8:
                case 9:
                    return [NSNumber numberWithInt:3];
                case 10:
                case 11:
                case 12:
                    return [NSNumber numberWithInt:4];
                case 13:
                case 14:
                case 15:
                    return [NSNumber numberWithInt:5];
                    
                default:
                    return [NSNumber numberWithInt:6];
            }
            break;
        case 15: // Magus
            switch (index.magus.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:1];
                case 4:
                case 5:
                case 6:
                    return [NSNumber numberWithInt:2];
                case 7:
                case 8:
                case 9:
                    return [NSNumber numberWithInt:3];
                case 10:
                case 11:
                case 12:
                    return [NSNumber numberWithInt:4];
                case 13:
                case 14:
                case 15:
                    return [NSNumber numberWithInt:5];
                    
                default:
                    return [NSNumber numberWithInt:6];
            }
            break;
        case 16: // Oracle
            switch (index.oracle.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:1];
                case 4:
                case 5:
                    return [NSNumber numberWithInt:2];
                case 6:
                case 7:
                    return [NSNumber numberWithInt:3];
                case 8:
                case 9:
                    return [NSNumber numberWithInt:4];
                case 10:
                case 11:
                    return [NSNumber numberWithInt:5];
                case 12:
                case 13:
                    return [NSNumber numberWithInt:6];
                case 14:
                case 15:
                    return [NSNumber numberWithInt:7];
                case 16:
                case 17:
                    return [NSNumber numberWithInt:8];
                    
                default:
                    return [NSNumber numberWithInt:9];
            }
            break;
        case 17: // Summoner
            switch (index.summoner.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                case 1:
                case 2:
                case 3:
                    return [NSNumber numberWithInt:1];
                case 4:
                case 5:
                case 6:
                    return [NSNumber numberWithInt:2];
                case 7:
                case 8:
                case 9:
                    return [NSNumber numberWithInt:3];
                case 10:
                case 11:
                case 12:
                    return [NSNumber numberWithInt:4];
                case 13:
                case 14:
                case 15:
                    return [NSNumber numberWithInt:5];
                    
                default:
                    return [NSNumber numberWithInt:6];
            }
            break;
        case 18: // Witch
            switch (index.witch.intValue) {
                case 0:
                    return [NSNumber numberWithInt:999];
                    break;
                case 1:
                case 2:
                    return [NSNumber numberWithInt:1];
                case 3:
                case 4:
                    return [NSNumber numberWithInt:2];
                case 5:
                case 6:
                    return [NSNumber numberWithInt:3];
                case 7:
                case 8:
                    return [NSNumber numberWithInt:4];
                case 9:
                case 10:
                    return [NSNumber numberWithInt:5];
                case 11:
                case 12:
                    return [NSNumber numberWithInt:6];
                case 13:
                case 14:
                    return [NSNumber numberWithInt:7];
                case 15:
                case 16:
                    return [NSNumber numberWithInt:8];
                    
                default:
                    return [NSNumber numberWithInt:9];
            }
            break;
            
            
        default:
            return [NSNumber numberWithInt:99];
    }
}

+ (NSNumber *)resolveClassSpellLevelToSpellsKnown:(ClassData *)index ClassID:(NSNumber *)classNumber SpellLevel:(NSNumber *)spellLevel {
    switch (classNumber.intValue) {
        case 1: // Bard
            switch (spellLevel.intValue) {
                case 0:
                    switch (index.bard.intValue) {
                        case 1:
                            return [NSNumber numberWithInt:4];
                        case 2:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 1:
                    switch (index.bard.intValue) {
                        case 1:
                            return [NSNumber numberWithInt:2];
                        case 2:
                            return [NSNumber numberWithInt:3];
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:4];
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 2:
                    switch (index.bard.intValue) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                            return [NSNumber numberWithInt:0];
                        case 4:
                            return [NSNumber numberWithInt:2];
                        case 5:
                            return [NSNumber numberWithInt:3];
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            return [NSNumber numberWithInt:4];
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 3:
                    switch (index.bard.intValue) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:0];
                        case 7:
                            return [NSNumber numberWithInt:2];
                        case 8:
                            return [NSNumber numberWithInt:3];
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                            return [NSNumber numberWithInt:4];
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 4:
                    switch (index.bard.intValue) {
                        case 10:
                            return [NSNumber numberWithInt:2];
                        case 11:
                            return [NSNumber numberWithInt:3];
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            return [NSNumber numberWithInt:4];
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                            return [NSNumber numberWithInt:5];
                        case 20:
                            return [NSNumber numberWithInt:6];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 5:
                    switch (index.bard.intValue) {
                        case 13:
                            return [NSNumber numberWithInt:2];
                        case 14:
                            return [NSNumber numberWithInt:3];
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                            return [NSNumber numberWithInt:4];
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 6:
                    switch (index.bard.intValue) {
                        case 16:
                            return [NSNumber numberWithInt:2];
                        case 17:
                            return [NSNumber numberWithInt:3];
                        case 18:
                        case 19:
                            return [NSNumber numberWithInt:4];
                        case 20:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
            }
        case 9: // Sorcerer
            switch (spellLevel.intValue) {
                case 0:
                    switch (index.sorcerer.intValue) {
                        case 1:
                            return [NSNumber numberWithInt:4];
                        case 2:
                        case 3:
                            return [NSNumber numberWithInt:5];
                        case 4:
                        case 5:
                            return [NSNumber numberWithInt:6];
                        case 6:
                        case 7:
                            return [NSNumber numberWithInt:7];
                        case 8:
                        case 9:
                            return [NSNumber numberWithInt:8];
                            
                        default:
                            return [NSNumber numberWithInt:9];
                    }
                case 1:
                    switch (index.sorcerer.intValue) {
                        case 1:
                        case 2:
                            return [NSNumber numberWithInt:2];
                        case 3:
                        case 4:
                            return [NSNumber numberWithInt:3];
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:4];
                            
                        default:
                            return [NSNumber numberWithInt:5];
                    }
                case 2:
                    switch (index.sorcerer.intValue) {
                        case 1:
                        case 2:
                        case 3:
                            return [NSNumber numberWithInt:0];
                        case 4:
                            return [NSNumber numberWithInt:1];
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:2];
                        case 7:
                        case 8:
                            return [NSNumber numberWithInt:3];
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:4];
                            
                        default:
                            return [NSNumber numberWithInt:5];
                    }
                case 3:
                    switch (index.sorcerer.intValue) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            return [NSNumber numberWithInt:0];
                        case 6:
                            return [NSNumber numberWithInt:1];
                        case 7:
                        case 8:
                            return [NSNumber numberWithInt:2];
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:4];
                    }
                case 4:
                    switch (index.sorcerer.intValue) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            return [NSNumber numberWithInt:0];
                        case 8:
                            return [NSNumber numberWithInt:1];
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:2];
                        case 11:
                        case 12:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:4];
                    }
                case 5:
                    switch (index.sorcerer.intValue) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            return [NSNumber numberWithInt:0];
                        case 10:
                            return [NSNumber numberWithInt:1];
                        case 11:
                        case 12:
                            return [NSNumber numberWithInt:2];
                        case 13:
                        case 14:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:4];
                    }
                case 6:
                    switch (index.sorcerer.intValue) {
                        case 12:
                            return [NSNumber numberWithInt:1];
                        case 13:
                        case 14:
                            return [NSNumber numberWithInt:2];
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 7:
                    switch (index.sorcerer.intValue) {
                        case 14:
                            return [NSNumber numberWithInt:1];
                        case 15:
                        case 16:
                            return [NSNumber numberWithInt:2];
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 8:
                    switch (index.sorcerer.intValue) {
                        case 16:
                            return [NSNumber numberWithInt:1];
                        case 17:
                        case 18:
                            return [NSNumber numberWithInt:2];
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 9:
                    switch (index.sorcerer.intValue) {
                        case 18:
                            return [NSNumber numberWithInt:1];
                        case 19:
                            return [NSNumber numberWithInt:2];
                        case 20:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            break;
                    }
                }
        //case 10: // Wizard (Starts with All 0, 3+IntMod First, Gains 2 per level
        case 14: // Inquisitor (Same as Bard)
            switch (spellLevel.intValue) {
                case 0:
                    switch (index.inquisitor.intValue) {
                        case 1:
                            return [NSNumber numberWithInt:4];
                        case 2:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 1:
                    switch (index.inquisitor.intValue) {
                        case 1:
                            return [NSNumber numberWithInt:2];
                        case 2:
                            return [NSNumber numberWithInt:3];
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:4];
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 2:
                    switch (index.inquisitor.intValue) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                            return [NSNumber numberWithInt:0];
                        case 4:
                            return [NSNumber numberWithInt:2];
                        case 5:
                            return [NSNumber numberWithInt:3];
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            return [NSNumber numberWithInt:4];
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 3:
                    switch (index.inquisitor.intValue) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:0];
                        case 7:
                            return [NSNumber numberWithInt:2];
                        case 8:
                            return [NSNumber numberWithInt:3];
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                            return [NSNumber numberWithInt:4];
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 4:
                    switch (index.inquisitor.intValue) {
                        case 10:
                            return [NSNumber numberWithInt:2];
                        case 11:
                            return [NSNumber numberWithInt:3];
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            return [NSNumber numberWithInt:4];
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                            return [NSNumber numberWithInt:5];
                        case 20:
                            return [NSNumber numberWithInt:6];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 5:
                    switch (index.inquisitor.intValue) {
                        case 13:
                            return [NSNumber numberWithInt:2];
                        case 14:
                            return [NSNumber numberWithInt:3];
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                            return [NSNumber numberWithInt:4];
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 6:
                    switch (index.inquisitor.intValue) {
                        case 16:
                            return [NSNumber numberWithInt:2];
                        case 17:
                            return [NSNumber numberWithInt:3];
                        case 18:
                        case 19:
                            return [NSNumber numberWithInt:4];
                        case 20:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
            }
        //case 15: // Magus (Starts with All 0, 3+IntMod First, Gains 2 per level)
        case 16: // Oracle (Same as Sorcerer)
            switch (spellLevel.intValue) {
                case 0:
                    switch (index.oracle.intValue) {
                        case 1:
                            return [NSNumber numberWithInt:4];
                        case 2:
                        case 3:
                            return [NSNumber numberWithInt:5];
                        case 4:
                        case 5:
                            return [NSNumber numberWithInt:6];
                        case 6:
                        case 7:
                            return [NSNumber numberWithInt:7];
                        case 8:
                        case 9:
                            return [NSNumber numberWithInt:8];
                            
                        default:
                            return [NSNumber numberWithInt:9];
                    }
                case 1:
                    switch (index.oracle.intValue) {
                        case 1:
                        case 2:
                            return [NSNumber numberWithInt:2];
                        case 3:
                        case 4:
                            return [NSNumber numberWithInt:3];
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:4];
                            
                        default:
                            return [NSNumber numberWithInt:5];
                    }
                case 2:
                    switch (index.oracle.intValue) {
                        case 1:
                        case 2:
                        case 3:
                            return [NSNumber numberWithInt:0];
                        case 4:
                            return [NSNumber numberWithInt:1];
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:2];
                        case 7:
                        case 8:
                            return [NSNumber numberWithInt:3];
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:4];
                            
                        default:
                            return [NSNumber numberWithInt:5];
                    }
                case 3:
                    switch (index.oracle.intValue) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            return [NSNumber numberWithInt:0];
                        case 6:
                            return [NSNumber numberWithInt:1];
                        case 7:
                        case 8:
                            return [NSNumber numberWithInt:2];
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:4];
                    }
                case 4:
                    switch (index.oracle.intValue) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            return [NSNumber numberWithInt:0];
                        case 8:
                            return [NSNumber numberWithInt:1];
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:2];
                        case 11:
                        case 12:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:4];
                    }
                case 5:
                    switch (index.oracle.intValue) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            return [NSNumber numberWithInt:0];
                        case 10:
                            return [NSNumber numberWithInt:1];
                        case 11:
                        case 12:
                            return [NSNumber numberWithInt:2];
                        case 13:
                        case 14:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:4];
                    }
                case 6:
                    switch (index.oracle.intValue) {
                        case 12:
                            return [NSNumber numberWithInt:1];
                        case 13:
                        case 14:
                            return [NSNumber numberWithInt:2];
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 7:
                    switch (index.oracle.intValue) {
                        case 14:
                            return [NSNumber numberWithInt:1];
                        case 15:
                        case 16:
                            return [NSNumber numberWithInt:2];
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 8:
                    switch (index.oracle.intValue) {
                        case 16:
                            return [NSNumber numberWithInt:1];
                        case 17:
                        case 18:
                            return [NSNumber numberWithInt:2];
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 9:
                    switch (index.oracle.intValue) {
                        case 18:
                            return [NSNumber numberWithInt:1];
                        case 19:
                            return [NSNumber numberWithInt:2];
                        case 20:
                            return [NSNumber numberWithInt:3];
                            
                        default:
                            break;
                    }
            }
        case 17: // Summoner (Same as Bard)
            switch (spellLevel.intValue) {
                case 0:
                    switch (index.summoner.intValue) {
                        case 1:
                            return [NSNumber numberWithInt:4];
                        case 2:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 1:
                    switch (index.summoner.intValue) {
                        case 1:
                            return [NSNumber numberWithInt:2];
                        case 2:
                            return [NSNumber numberWithInt:3];
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:4];
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 2:
                    switch (index.summoner.intValue) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                            return [NSNumber numberWithInt:0];
                        case 4:
                            return [NSNumber numberWithInt:2];
                        case 5:
                            return [NSNumber numberWithInt:3];
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            return [NSNumber numberWithInt:4];
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 3:
                    switch (index.summoner.intValue) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            return [NSNumber numberWithInt:0];
                        case 7:
                            return [NSNumber numberWithInt:2];
                        case 8:
                            return [NSNumber numberWithInt:3];
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                            return [NSNumber numberWithInt:4];
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
                case 4:
                    switch (index.summoner.intValue) {
                        case 10:
                            return [NSNumber numberWithInt:2];
                        case 11:
                            return [NSNumber numberWithInt:3];
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            return [NSNumber numberWithInt:4];
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                            return [NSNumber numberWithInt:5];
                        case 20:
                            return [NSNumber numberWithInt:6];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 5:
                    switch (index.summoner.intValue) {
                        case 13:
                            return [NSNumber numberWithInt:2];
                        case 14:
                            return [NSNumber numberWithInt:3];
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                            return [NSNumber numberWithInt:4];
                        case 19:
                        case 20:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:0];
                    }
                case 6:
                    switch (index.summoner.intValue) {
                        case 16:
                            return [NSNumber numberWithInt:2];
                        case 17:
                            return [NSNumber numberWithInt:3];
                        case 18:
                        case 19:
                            return [NSNumber numberWithInt:4];
                        case 20:
                            return [NSNumber numberWithInt:5];
                            
                        default:
                            return [NSNumber numberWithInt:6];
                    }
            }
        //case 18: // Witch (Starts with All 0, 3+IntMod First, Gains 2 per level)
            
        default:
            return [NSNumber numberWithInt:0];
    }
}

+ (UIImage *)resolveClassToImage:(NSNumber *)index {
    NSString * class = [Helper resolveClassToString:index];
    NSString * file = [NSString stringWithFormat:@"%@_Icon.png", class];
    return [UIImage imageNamed:file];
}

+ (NSString *)resolveDietyToString:(NSNumber *)index {
    switch (index.intValue) {
        case 0:
            return @"Abadar";
        case 1:
            return @"Asmodeus";
        case 2:
            return @"Calistria";
        case 3:
            return @"Cayden Cailean";
        case 4:
            return @"Desna";
        case 5:
            return @"Erastil";
        case 6:
            return @"Gorum";
        case 7:
            return @"Gozreh";
        case 8:
            return @"Iomedae";
        case 9:
            return @"Irori";
        case 10:
            return @"Lamashtu";
        case 11:
            return @"Nethys";
        case 12:
            return @"Norgorber";
        case 13:
            return @"Pharasma";
        case 14:
            return @"Rovagug";
        case 15:
            return @"Sarenrae";
        case 16:
            return @"Shelyn";
        case 17:
            return @"Torag";
        case 18:
            return @"Urgathoa";
        case 19:
            return @"Zon-Kuthon";
        case 20:
            return @"Achaekek";
        case 21:
            return @"Apsu";
        case 22:
            return @"Azathoth";
        case 23:
            return @"Besmara";
        case 24:
            return @"Chamidu";
        case 25:
            return @"Dahak";
        case 26:
            return @"Droskar";
        case 27:
            return @"Ghlaunder";
        case 28:
            return @"Groetus";
        case 29:
            return @"Kurgess";
        case 30:
            return @"Lissala";
        case 31:
            return @"Milani";
        case 32:
            return @"Peacock";
        case 33:
            return @"Spirit";
        case 34:
            return @"Sivanah";
        case 35:
            return @"Ydersius";
        case 36:
            return @"Zyphus";
            
        default:
            return @"Unknown";
    }
}

+ (NSString *)resolveGenderToString:(NSNumber *)index {
    switch (index.intValue) {
        case 0:
            return @"Male";
        case 1:
            return @"Female";
            
        default:
            return @"Unknown";
    }
}

+ (NSNumber *)resolveLevelToExperience:(NSNumber *)index Progression:(NSNumber *)progression {
    NSNumber * previous;
    switch (progression.intValue) {
        case 0:
            switch (index.intValue) {
                case 0:
                    return [NSNumber numberWithInt:0];
                case 1:
                    return [NSNumber numberWithInt:3000];
                case 2:
                    return [NSNumber numberWithInt:7500];
                case 3:
                    return [NSNumber numberWithInt:14000];
                case 4:
                    return [NSNumber numberWithInt:23000];
                case 5:
                    return [NSNumber numberWithInt:35000];
                case 6:
                    return [NSNumber numberWithInt:53000];
                case 7:
                    return [NSNumber numberWithInt:77000];
                case 8:
                    return [NSNumber numberWithInt:115000];
                case 9:
                    return [NSNumber numberWithInt:160000];
                case 10:
                    return [NSNumber numberWithInt:235000];
                case 11:
                    return [NSNumber numberWithInt:330000];
                case 12:
                    return [NSNumber numberWithInt:475000];
                case 13:
                    return [NSNumber numberWithInt:665000];
                case 14:
                    return [NSNumber numberWithInt:955000];
                case 15:
                    return [NSNumber numberWithInt:1350000];
                case 16:
                    return [NSNumber numberWithInt:1900000];
                case 17:
                    return [NSNumber numberWithInt:2700000];
                case 18:
                    return [NSNumber numberWithInt:3850000];
                case 19:
                    return [NSNumber numberWithInt:5350000];
                    
                default:
                    previous = [self resolveLevelToExperience:[NSNumber numberWithInt:index.intValue - 1] Progression:progression];
                    return [NSNumber numberWithInt:previous.intValue * 2];
            }
        case 1:
            switch (index.intValue) {
                case 0:
                    return [NSNumber numberWithInt:0];
                case 1:
                    return [NSNumber numberWithInt:2000];
                case 2:
                    return [NSNumber numberWithInt:5000];
                case 3:
                    return [NSNumber numberWithInt:9000];
                case 4:
                    return [NSNumber numberWithInt:15000];
                case 5:
                    return [NSNumber numberWithInt:23000];
                case 6:
                    return [NSNumber numberWithInt:35000];
                case 7:
                    return [NSNumber numberWithInt:51000];
                case 8:
                    return [NSNumber numberWithInt:75000];
                case 9:
                    return [NSNumber numberWithInt:105000];
                case 10:
                    return [NSNumber numberWithInt:155000];
                case 11:
                    return [NSNumber numberWithInt:220000];
                case 12:
                    return [NSNumber numberWithInt:315000];
                case 13:
                    return [NSNumber numberWithInt:445000];
                case 14:
                    return [NSNumber numberWithInt:635000];
                case 15:
                    return [NSNumber numberWithInt:890000];
                case 16:
                    return [NSNumber numberWithInt:1300000];
                case 17:
                    return [NSNumber numberWithInt:1800000];
                case 18:
                    return [NSNumber numberWithInt:2550000];
                case 19:
                    return [NSNumber numberWithInt:3600000];
                    
                default:
                    previous = [self resolveLevelToExperience:[NSNumber numberWithInt:index.intValue - 1] Progression:progression];
                    return [NSNumber numberWithInt:previous.intValue * 2];
            }
        case 2:
            switch (index.intValue) {
                case 0:
                    return [NSNumber numberWithInt:0];
                case 1:
                    return [NSNumber numberWithInt:1300];
                case 2:
                    return [NSNumber numberWithInt:3300];
                case 3:
                    return [NSNumber numberWithInt:6000];
                case 4:
                    return [NSNumber numberWithInt:10000];
                case 5:
                    return [NSNumber numberWithInt:15000];
                case 6:
                    return [NSNumber numberWithInt:23000];
                case 7:
                    return [NSNumber numberWithInt:34000];
                case 8:
                    return [NSNumber numberWithInt:50000];
                case 9:
                    return [NSNumber numberWithInt:71000];
                case 10:
                    return [NSNumber numberWithInt:105000];
                case 11:
                    return [NSNumber numberWithInt:145000];
                case 12:
                    return [NSNumber numberWithInt:210000];
                case 13:
                    return [NSNumber numberWithInt:295000];
                case 14:
                    return [NSNumber numberWithInt:425000];
                case 15:
                    return [NSNumber numberWithInt:600000];
                case 16:
                    return [NSNumber numberWithInt:850000];
                case 17:
                    return [NSNumber numberWithInt:1200000];
                case 18:
                    return [NSNumber numberWithInt:1700000];
                case 19:
                    return [NSNumber numberWithInt:2400000];
                    
                default:
                    previous = [self resolveLevelToExperience:[NSNumber numberWithInt:index.intValue - 1] Progression:progression];
                    return [NSNumber numberWithInt:previous.intValue * 2];
            }
            
        default:
            return 0;
    }
}

+ (NSString *)resolveRaceToString:(NSNumber *)index {
    switch (index.intValue) {
        case 0:
            return @"Dwarf";
        case 1:
            return @"Elf";
        case 2:
            return @"Gnome";
        case 3:
            return @"Half-Elf";
        case 4:
            return @"Halfling";
        case 5:
            return @"Half-Orc";
        case 6:
            return @"Human";
            
        default:
            return @"Unknown";
    }
}

+ (NSString *)resolveRowToSkillString:(NSInteger)index {
    switch (index) {
        case 0:
            return @"Acrobatics";
        case 1:
            return @"Appraise";
        case 2:
            return @"Bluff";
        case 3:
            return @"Climb";
        case 4:
            return @"Diplomacy";
        case 5:
            return @"Disable Device";
        case 6:
            return @"Disguise";
        case 7:
            return @"Escape Artist";
        case 8:
            return @"Fly";
        case 9:
            return @"Handle Animal";
        case 10:
            return @"Heal";
        case 11:
            return @"Intimidate";
        case 12:
            return @"Knowledge (Arcana)";
        case 13:
            return @"Knowledge (Dungeoneering)";
        case 14:
            return @"Knowledge (Engineering)";
        case 15:
            return @"Knowledge (Geography)";
        case 16:
            return @"Knowledge (History)";
        case 17:
            return @"Knowledge (Local)";
        case 18:
            return @"Knowledge (Nature)";
        case 19:
            return @"Knowledge (Nobility)";
        case 20:
            return @"Knowledge (Planes)";
        case 21:
            return @"Knowledge (Religion)";
        case 22:
            return @"Linguistics";
        case 23:
            return @"Perception";
        case 24:
            return @"Ride";
        case 25:
            return @"Sense Motive";
        case 26:
            return @"Sleight of Hand";
        case 27:
            return @"Spellcraft";
        case 28:
            return @"Stealth";
        case 29:
            return @"Survival";
        case 30:
            return @"Swim";
        case 31:
            return @"Use Magic Device";
            
        default:
            return @"Unknown";
    }
}

+ (NSNumber *)resolveSkillFromSkillData:(SkillData *)index Index:(NSInteger)skill {
    switch (skill) {
        case 0:
            return index.acrobatics;
        case 1:
            return index.appraise;
        case 2:
            return index.bluff;
        case 3:
            return index.climb;
        case 4:
            return index.diplomacy;
        case 5:
            return index.disable;
        case 6:
            return index.disguise;
        case 7:
            return index.escape;
        case 8:
            return index.fly;
        case 9:
            return index.handle;
        case 10:
            return index.heal;
        case 11:
            return index.intimidate;
        case 12:
            return index.know_arcana;
        case 13:
            return index.know_dungeon;
        case 14:
            return index.know_engine;
        case 15:
            return index.know_geography;
        case 16:
            return index.know_history;
        case 17:
            return index.know_local;
        case 18:
            return index.know_nature;
        case 19:
            return index.know_nobility;
        case 20:
            return index.know_planes;
        case 21:
            return index.know_religion;
        case 22:
            return index.linguistics;
        case 23:
            return index.perception;
        case 24:
            return index.ride;
        case 25:
            return index.sense;
        case 26:
            return index.sleight;
        case 27:
            return index.spellcraft;
        case 28:
            return index.stealth;
        case 29:
            return index.survival;
        case 30:
            return index.swim;
        case 31:
            return index.use_magic;
            
        default:
            return [NSNumber numberWithInt:0];
    }
}

@end
