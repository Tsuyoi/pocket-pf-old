//
//  CharacterBasicInfoTabViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 3/29/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CharacterInfoTabBarController.h"
#import "CharacterData.h"
#import "ClassData.h"

@interface CharacterBasicInfoTabViewController : UITableViewController <UIAlertViewDelegate>

@property (nonatomic, strong) CharacterInfoTabBarController * parent;

@property (nonatomic, strong) IBOutlet UILabel * lblName;
@property (nonatomic, strong) IBOutlet UILabel * lblRace;
@property (nonatomic, strong) IBOutlet UILabel * lblSize;
@property (nonatomic, strong) IBOutlet UILabel * lblLook;
@property (nonatomic, strong) IBOutlet UILabel * lblCulture;

@property (nonatomic, strong) IBOutlet UIImageView * infoImage;

@property (nonatomic, strong) IBOutlet UIProgressView * expProgress;
@property (nonatomic, strong) IBOutlet UILabel * expValues;

@property (nonatomic, strong) IBOutlet UIProgressView * hpProgress;
@property (nonatomic, strong) IBOutlet UILabel * hpValues;

@property (nonatomic, strong) IBOutlet UILabel * levelBarbarian;
@property (nonatomic, strong) IBOutlet UILabel * levelBard;
@property (nonatomic, strong) IBOutlet UILabel * levelCleric;
@property (nonatomic, strong) IBOutlet UILabel * levelDruid;
@property (nonatomic, strong) IBOutlet UILabel * levelFighter;
@property (nonatomic, strong) IBOutlet UILabel * levelMonk;
@property (nonatomic, strong) IBOutlet UILabel * levelPaladin;
@property (nonatomic, strong) IBOutlet UILabel * levelRanger;
@property (nonatomic, strong) IBOutlet UILabel * levelRogue;
@property (nonatomic, strong) IBOutlet UILabel * levelSorcerer;
@property (nonatomic, strong) IBOutlet UILabel * levelWizard;
@property (nonatomic, strong) IBOutlet UILabel * levelAlchemist;
@property (nonatomic, strong) IBOutlet UILabel * levelCavalier;
@property (nonatomic, strong) IBOutlet UILabel * levelGunslinger;
@property (nonatomic, strong) IBOutlet UILabel * levelInquisitor;
@property (nonatomic, strong) IBOutlet UILabel * levelMagus;
@property (nonatomic, strong) IBOutlet UILabel * levelOracle;
@property (nonatomic, strong) IBOutlet UILabel * levelSummoner;
@property (nonatomic, strong) IBOutlet UILabel * levelWitch;

@property (nonatomic, strong) IBOutlet UILabel * scoreStr;
@property (nonatomic, strong) IBOutlet UILabel * scoreDex;
@property (nonatomic, strong) IBOutlet UILabel * scoreCon;
@property (nonatomic, strong) IBOutlet UILabel * scoreInt;
@property (nonatomic, strong) IBOutlet UILabel * scoreWis;
@property (nonatomic, strong) IBOutlet UILabel * scoreCha;

@property (nonatomic, strong) IBOutlet UILabel * defAC;
@property (nonatomic, strong) IBOutlet UILabel * defFort;
@property (nonatomic, strong) IBOutlet UILabel * defRef;
@property (nonatomic, strong) IBOutlet UILabel * defWill;
@property (nonatomic, strong) IBOutlet UILabel * defSpellRes;

@property (nonatomic, strong) IBOutlet UILabel * offBAB;
@property (nonatomic, strong) IBOutlet UILabel * offCMB;
@property (nonatomic, strong) IBOutlet UILabel * offCMD;
@property (nonatomic, strong) IBOutlet UILabel * offInit;

@end
