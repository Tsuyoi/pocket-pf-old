//
//  FeatSource.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "FeatSource.h"


@implementation FeatSource

@dynamic name;
@dynamic prereq;
@dynamic summary;
@dynamic type;

@end
