//
//  MagicItemData.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "MagicItemData.h"
#import "CharacterData.h"


@implementation MagicItemData

@dynamic abjuration;
@dynamic aura;
@dynamic aura_strength;
@dynamic cl;
@dynamic conjuration;
@dynamic cost;
@dynamic cost_value;
@dynamic desc;
@dynamic divination;
@dynamic enchantment;
@dynamic evocation;
@dynamic group;
@dynamic majorartifactflag;
@dynamic minorartifactflag;
@dynamic name;
@dynamic necromancy;
@dynamic price;
@dynamic price_value;
@dynamic requirements;
@dynamic slot;
@dynamic transmutation;
@dynamic weight_value;
@dynamic character;

@end
