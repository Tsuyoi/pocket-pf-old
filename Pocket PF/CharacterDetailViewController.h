//
//  CharacterDetailViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Character.h"
#import "AbilityPickerViewController.h"
#import "AlignmentPickerViewController.h"
#import "RacePickerViewController.h"
#import "ClassPickerViewController.h"

@class CharacterDetailViewController;

@protocol CharacterDetailViewControllerDelegate <NSObject>

- (void)characterDetailViewControllerDidCancel:(CharacterDetailViewController *)controller;
- (void)characterDetailViewController:(CharacterDetailViewController *)controller didAddCharacter:(Character *)character;

@end

@interface CharacterDetailViewController : UITableViewController <AbilityPickerViewControllerDelegate, AlignmentPickerViewControllerDelegate, RacePickerViewControllerDelegate, ClassPickerViewControllerDelegate>

@property (weak, nonatomic) id <CharacterDetailViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITextField *nameText;
@property (strong, nonatomic) IBOutlet UILabel *alignmentLabel;
@property (strong, nonatomic) IBOutlet UILabel *raceLabel;
@property (strong, nonatomic) IBOutlet UILabel *classLabel;
@property (strong, nonatomic) IBOutlet UILabel *abilityScoreLabel;

@property (strong, nonatomic) IBOutlet UILabel *strengthTotal;
@property (strong, nonatomic) IBOutlet UILabel *dexterityTotal;
@property (strong, nonatomic) IBOutlet UILabel *constitutionTotal;
@property (strong, nonatomic) IBOutlet UILabel *intelligenceTotal;
@property (strong, nonatomic) IBOutlet UILabel *wisdomTotal;
@property (strong, nonatomic) IBOutlet UILabel *charismaTotal;

@property (strong, nonatomic) IBOutlet UILabel *pointsTotal;
@property (strong, nonatomic) IBOutlet UISegmentedControl *genderSelect;

- (IBAction)cancel:(id)sender;
- (IBAction)save:(id)sender;

- (IBAction)updateStrength:(id)sender;
- (IBAction)updateDexterity:(id)sender;
- (IBAction)updateConstitution:(id)sender;
- (IBAction)updateIntelligence:(id)sender;
- (IBAction)updateWisdom:(id)sender;
- (IBAction)updateCharisma:(id)sender;

@end
