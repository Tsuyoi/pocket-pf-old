//
//  AlignmentPickerViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AlignmentPickerViewController;

@protocol AlignmentPickerViewControllerDelegate <NSObject>

- (void)alignmentPickerViewController:(AlignmentPickerViewController *)controller didSelectAlignment:(NSString *)alignmentName alignmentIndex:(int)index;

@end

@interface AlignmentPickerViewController : UITableViewController

@property (weak, nonatomic) id <AlignmentPickerViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString * alignment;

@end
