//
//  CharacterSkillTableViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 3/31/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//
#import <CoreData/CoreData.h>
#import "CharacterSkillTableViewController.h"
#import "Helper.h"
#import "SkillListCell.h"

@implementation CharacterSkillTableViewController {
    NSMutableArray * skillBonus;
    int totalSkillRanks;
    int skillRanksSpent;
    int totalCharacterLevel;
}

@synthesize parent;
@synthesize charIndex;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    skillBonus = [[NSMutableArray alloc] init];
    NSNumber * temp_0 = [NSNumber numberWithInt:0];
    for (int i = 0; i < 32; i++) {
        [skillBonus addObject:temp_0];
    }
    NSNumber * temp_1 = [NSNumber numberWithInt:3];
    if (self.parent.classes.alchemist.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:1];
        [skillBonus insertObject:temp_1 atIndex:5];
        [skillBonus insertObject:temp_1 atIndex:8];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:26];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:29];
        [skillBonus insertObject:temp_1 atIndex:31];
    }
    if (self.parent.classes.barbarian.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:0];
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:9];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:29];
        [skillBonus insertObject:temp_1 atIndex:30];
    }
    if (self.parent.classes.bard.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:0];
        [skillBonus insertObject:temp_1 atIndex:1];
        [skillBonus insertObject:temp_1 atIndex:2];
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:4];
        [skillBonus insertObject:temp_1 atIndex:6];
        [skillBonus insertObject:temp_1 atIndex:7];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:13];
        [skillBonus insertObject:temp_1 atIndex:14];
        [skillBonus insertObject:temp_1 atIndex:15];
        [skillBonus insertObject:temp_1 atIndex:16];
        [skillBonus insertObject:temp_1 atIndex:17];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:19];
        [skillBonus insertObject:temp_1 atIndex:20];
        [skillBonus insertObject:temp_1 atIndex:21];
        [skillBonus insertObject:temp_1 atIndex:22];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:25];
        [skillBonus insertObject:temp_1 atIndex:26];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:28];
        [skillBonus insertObject:temp_1 atIndex:31];
    }
    if (self.parent.classes.cavalier.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:2];
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:4];
        [skillBonus insertObject:temp_1 atIndex:9];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:25];
        [skillBonus insertObject:temp_1 atIndex:30];
    }
    if (self.parent.classes.cleric.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:1];
        [skillBonus insertObject:temp_1 atIndex:4];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:16];
        [skillBonus insertObject:temp_1 atIndex:19];
        [skillBonus insertObject:temp_1 atIndex:20];
        [skillBonus insertObject:temp_1 atIndex:21];
        [skillBonus insertObject:temp_1 atIndex:22];
        [skillBonus insertObject:temp_1 atIndex:25];
        [skillBonus insertObject:temp_1 atIndex:27];
    }
    if (self.parent.classes.druid.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:8];
        [skillBonus insertObject:temp_1 atIndex:9];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:15];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:29];
        [skillBonus insertObject:temp_1 atIndex:30];
    }
    if (self.parent.classes.fighter.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:9];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:13];
        [skillBonus insertObject:temp_1 atIndex:14];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:29];
        [skillBonus insertObject:temp_1 atIndex:30];
    }
    if (self.parent.classes.gunslinger.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:0];
        [skillBonus insertObject:temp_1 atIndex:2];
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:9];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:14];
        [skillBonus insertObject:temp_1 atIndex:17];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:26];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:30];
    }
    if (self.parent.classes.inquisitor.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:2];
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:4];
        [skillBonus insertObject:temp_1 atIndex:6];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:13];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:20];
        [skillBonus insertObject:temp_1 atIndex:21];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:25];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:28];
        [skillBonus insertObject:temp_1 atIndex:29];
        [skillBonus insertObject:temp_1 atIndex:30];
    }
    if (self.parent.classes.magus.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:8];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:13];
        [skillBonus insertObject:temp_1 atIndex:20];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:30];
        [skillBonus insertObject:temp_1 atIndex:31];
    }
    if (self.parent.classes.monk.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:0];
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:7];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:16];
        [skillBonus insertObject:temp_1 atIndex:21];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:25];
        [skillBonus insertObject:temp_1 atIndex:28];
        [skillBonus insertObject:temp_1 atIndex:30];
    }
    if (self.parent.classes.oracle.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:4];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:16];
        [skillBonus insertObject:temp_1 atIndex:20];
        [skillBonus insertObject:temp_1 atIndex:21];
        [skillBonus insertObject:temp_1 atIndex:25];
        [skillBonus insertObject:temp_1 atIndex:27];
    }
    if (self.parent.classes.paladin.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:4];
        [skillBonus insertObject:temp_1 atIndex:9];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:19];
        [skillBonus insertObject:temp_1 atIndex:21];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:25];
        [skillBonus insertObject:temp_1 atIndex:27];
    }
    if (self.parent.classes.ranger.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:9];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:13];
        [skillBonus insertObject:temp_1 atIndex:15];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:28];
        [skillBonus insertObject:temp_1 atIndex:29];
        [skillBonus insertObject:temp_1 atIndex:30];
    }
    if (self.parent.classes.rogue.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:0];
        [skillBonus insertObject:temp_1 atIndex:1];
        [skillBonus insertObject:temp_1 atIndex:2];
        [skillBonus insertObject:temp_1 atIndex:3];
        [skillBonus insertObject:temp_1 atIndex:4];
        [skillBonus insertObject:temp_1 atIndex:5];
        [skillBonus insertObject:temp_1 atIndex:6];
        [skillBonus insertObject:temp_1 atIndex:7];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:13];
        [skillBonus insertObject:temp_1 atIndex:17];
        [skillBonus insertObject:temp_1 atIndex:22];
        [skillBonus insertObject:temp_1 atIndex:23];
        [skillBonus insertObject:temp_1 atIndex:25];
        [skillBonus insertObject:temp_1 atIndex:26];
        [skillBonus insertObject:temp_1 atIndex:28];
        [skillBonus insertObject:temp_1 atIndex:30];
        [skillBonus insertObject:temp_1 atIndex:31];
    }
    if (self.parent.classes.sorcerer.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:1];
        [skillBonus insertObject:temp_1 atIndex:2];
        [skillBonus insertObject:temp_1 atIndex:8];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:31];
    }
    if (self.parent.classes.summoner.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:8];
        [skillBonus insertObject:temp_1 atIndex:9];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:13];
        [skillBonus insertObject:temp_1 atIndex:14];
        [skillBonus insertObject:temp_1 atIndex:15];
        [skillBonus insertObject:temp_1 atIndex:16];
        [skillBonus insertObject:temp_1 atIndex:17];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:19];
        [skillBonus insertObject:temp_1 atIndex:20];
        [skillBonus insertObject:temp_1 atIndex:21];
        [skillBonus insertObject:temp_1 atIndex:22];
        [skillBonus insertObject:temp_1 atIndex:24];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:31];
    }
    if (self.parent.classes.witch.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:8];
        [skillBonus insertObject:temp_1 atIndex:10];
        [skillBonus insertObject:temp_1 atIndex:11];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:16];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:20];
        [skillBonus insertObject:temp_1 atIndex:27];
        [skillBonus insertObject:temp_1 atIndex:31];
    }
    if (self.parent.classes.wizard.intValue > 0) {
        [skillBonus insertObject:temp_1 atIndex:1];
        [skillBonus insertObject:temp_1 atIndex:8];
        [skillBonus insertObject:temp_1 atIndex:12];
        [skillBonus insertObject:temp_1 atIndex:13];
        [skillBonus insertObject:temp_1 atIndex:14];
        [skillBonus insertObject:temp_1 atIndex:15];
        [skillBonus insertObject:temp_1 atIndex:16];
        [skillBonus insertObject:temp_1 atIndex:17];
        [skillBonus insertObject:temp_1 atIndex:18];
        [skillBonus insertObject:temp_1 atIndex:19];
        [skillBonus insertObject:temp_1 atIndex:20];
        [skillBonus insertObject:temp_1 atIndex:21];
        [skillBonus insertObject:temp_1 atIndex:22];
        [skillBonus insertObject:temp_1 atIndex:27];
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)setSkillsBadge
{
    totalCharacterLevel = 0;
    totalCharacterLevel += self.parent.classes.alchemist.intValue;
    totalCharacterLevel += self.parent.classes.barbarian.intValue;
    totalCharacterLevel += self.parent.classes.bard.intValue;
    totalCharacterLevel += self.parent.classes.cavalier.intValue;
    totalCharacterLevel += self.parent.classes.cleric.intValue;
    totalCharacterLevel += self.parent.classes.druid.intValue;
    totalCharacterLevel += self.parent.classes.fighter.intValue;
    totalCharacterLevel += self.parent.classes.gunslinger.intValue;
    totalCharacterLevel += self.parent.classes.inquisitor.intValue;
    totalCharacterLevel += self.parent.classes.magus.intValue;
    totalCharacterLevel += self.parent.classes.monk.intValue;
    totalCharacterLevel += self.parent.classes.oracle.intValue;
    totalCharacterLevel += self.parent.classes.paladin.intValue;
    totalCharacterLevel += self.parent.classes.ranger.intValue;
    totalCharacterLevel += self.parent.classes.rogue.intValue;
    totalCharacterLevel += self.parent.classes.sorcerer.intValue;
    totalCharacterLevel += self.parent.classes.summoner.intValue;
    totalCharacterLevel += self.parent.classes.witch.intValue;
    totalCharacterLevel += self.parent.classes.wizard.intValue;
    totalSkillRanks = 0;
    int intMod = (self.parent.character.intel.intValue / 2) - 5;
    totalSkillRanks += self.parent.classes.alchemist.intValue * (4 + intMod);
    totalSkillRanks += self.parent.classes.barbarian.intValue * (4 + intMod);
    totalSkillRanks += self.parent.classes.bard.intValue * (6 + intMod);
    totalSkillRanks += self.parent.classes.cavalier.intValue * (4 + intMod);
    totalSkillRanks += self.parent.classes.cleric.intValue * (2 + intMod);
    totalSkillRanks += self.parent.classes.druid.intValue * (4 + intMod);
    totalSkillRanks += self.parent.classes.fighter.intValue * (2 + intMod);
    totalSkillRanks += self.parent.classes.gunslinger.intValue * (4 + intMod);
    totalSkillRanks += self.parent.classes.inquisitor.intValue * (6 + intMod);
    totalSkillRanks += self.parent.classes.magus.intValue * (2 + intMod);
    totalSkillRanks += self.parent.classes.monk.intValue * (4 + intMod);
    totalSkillRanks += self.parent.classes.oracle.intValue * (4 + intMod);
    totalSkillRanks += self.parent.classes.paladin.intValue * (2 + intMod);
    totalSkillRanks += self.parent.classes.ranger.intValue * (6 + intMod);
    totalSkillRanks += self.parent.classes.rogue.intValue * (8 + intMod);
    totalSkillRanks += self.parent.classes.sorcerer.intValue * (2 + intMod);
    totalSkillRanks += self.parent.classes.summoner.intValue * (2 + intMod);
    totalSkillRanks += self.parent.classes.witch.intValue * (2 + intMod);
    totalSkillRanks += self.parent.classes.wizard.intValue * (2 + intMod);
    skillRanksSpent = 0;
    skillRanksSpent += self.parent.skills.acrobatics.intValue;
    skillRanksSpent += self.parent.skills.appraise.intValue;
    skillRanksSpent += self.parent.skills.bluff.intValue;
    skillRanksSpent += self.parent.skills.climb.intValue;
    skillRanksSpent += self.parent.skills.diplomacy.intValue;
    skillRanksSpent += self.parent.skills.disable.intValue;
    skillRanksSpent += self.parent.skills.disguise.intValue;
    skillRanksSpent += self.parent.skills.escape.intValue;
    skillRanksSpent += self.parent.skills.fly.intValue;
    skillRanksSpent += self.parent.skills.handle.intValue;
    skillRanksSpent += self.parent.skills.heal.intValue;
    skillRanksSpent += self.parent.skills.intimidate.intValue;
    skillRanksSpent += self.parent.skills.know_arcana.intValue;
    skillRanksSpent += self.parent.skills.know_dungeon.intValue;
    skillRanksSpent += self.parent.skills.know_engine.intValue;
    skillRanksSpent += self.parent.skills.know_geography.intValue;
    skillRanksSpent += self.parent.skills.know_history.intValue;
    skillRanksSpent += self.parent.skills.know_local.intValue;
    skillRanksSpent += self.parent.skills.know_nature.intValue;
    skillRanksSpent += self.parent.skills.know_nobility.intValue;
    skillRanksSpent += self.parent.skills.know_planes.intValue;
    skillRanksSpent += self.parent.skills.know_religion.intValue;
    skillRanksSpent += self.parent.skills.linguistics.intValue;
    skillRanksSpent += self.parent.skills.perception.intValue;
    skillRanksSpent += self.parent.skills.ride.intValue;
    skillRanksSpent += self.parent.skills.sense.intValue;
    skillRanksSpent += self.parent.skills.sleight.intValue;
    skillRanksSpent += self.parent.skills.spellcraft.intValue;
    skillRanksSpent += self.parent.skills.stealth.intValue;
    skillRanksSpent += self.parent.skills.survival.intValue;
    skillRanksSpent += self.parent.skills.swim.intValue;
    skillRanksSpent += self.parent.skills.use_magic.intValue;
    if (totalSkillRanks - skillRanksSpent > 0)
        [[self tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%d", totalSkillRanks - skillRanksSpent]];
    else
        [[self tabBarItem] setBadgeValue:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setSkillsBadge];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 32;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SkillCell";
    
    SkillListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SkillListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSNumber * ranks = [Helper resolveSkillFromSkillData:self.parent.skills Index:indexPath.row];
    NSNumber * bonus = [NSNumber numberWithInt:0];
    if (ranks.intValue > 0)
        bonus = [skillBonus objectAtIndex:indexPath.row];
    
    cell.skillName.text = [Helper resolveRowToSkillString:indexPath.row];
    if (bonus.intValue > 0)
        cell.skillTotal.text = [NSString stringWithFormat:@"Total: %d (%@ Ranks + %@ Class Bonus)", ranks.intValue + bonus.intValue, ranks, bonus];
    else
        cell.skillTotal.text = [NSString stringWithFormat:@"Total: %d (%@ Ranks)", ranks.intValue, ranks];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    UIAlertView * alert;
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    if (skillRanksSpent < totalSkillRanks) {
        switch (indexPath.row) {
            case 0:
                if (self.parent.skills.acrobatics.intValue < totalCharacterLevel) {
                    self.parent.skills.acrobatics = [NSNumber numberWithInt:self.parent.skills.acrobatics.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 1:
                if (self.parent.skills.appraise.intValue < totalCharacterLevel) {
                    self.parent.skills.appraise = [NSNumber numberWithInt:self.parent.skills.appraise.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 2:
                if (self.parent.skills.bluff.intValue < totalCharacterLevel) {
                    self.parent.skills.bluff = [NSNumber numberWithInt:self.parent.skills.bluff.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 3:
                if (self.parent.skills.climb.intValue < totalCharacterLevel) {
                    self.parent.skills.climb = [NSNumber numberWithInt:self.parent.skills.climb.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 4:
                if (self.parent.skills.diplomacy.intValue < totalCharacterLevel) {
                    self.parent.skills.diplomacy = [NSNumber numberWithInt:self.parent.skills.diplomacy.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 5:
                if (self.parent.skills.disable.intValue < totalCharacterLevel) {
                    self.parent.skills.disable = [NSNumber numberWithInt:self.parent.skills.disable.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 6:
                if (self.parent.skills.disguise.intValue < totalCharacterLevel) {
                    self.parent.skills.disguise = [NSNumber numberWithInt:self.parent.skills.disguise.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 7:
                if (self.parent.skills.escape.intValue < totalCharacterLevel) {
                    self.parent.skills.escape = [NSNumber numberWithInt:self.parent.skills.escape.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 8:
                if (self.parent.skills.fly.intValue < totalCharacterLevel) {
                    self.parent.skills.fly = [NSNumber numberWithInt:self.parent.skills.fly.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 9:
                if (self.parent.skills.handle.intValue < totalCharacterLevel) {
                    self.parent.skills.handle = [NSNumber numberWithInt:self.parent.skills.handle.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 10:
                if (self.parent.skills.heal.intValue < totalCharacterLevel) {
                    self.parent.skills.heal = [NSNumber numberWithInt:self.parent.skills.heal.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 11:
                if (self.parent.skills.intimidate.intValue < totalCharacterLevel) {
                    self.parent.skills.intimidate = [NSNumber numberWithInt:self.parent.skills.intimidate.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 12:
                if (self.parent.skills.know_arcana.intValue < totalCharacterLevel) {
                    self.parent.skills.know_arcana = [NSNumber numberWithInt:self.parent.skills.know_arcana.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 13:
                if (self.parent.skills.know_dungeon.intValue < totalCharacterLevel) {
                    self.parent.skills.know_dungeon = [NSNumber numberWithInt:self.parent.skills.know_dungeon.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 14:
                if (self.parent.skills.know_engine.intValue < totalCharacterLevel) {
                    self.parent.skills.know_engine = [NSNumber numberWithInt:self.parent.skills.know_engine.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 15:
                if (self.parent.skills.know_geography.intValue < totalCharacterLevel) {
                    self.parent.skills.know_geography = [NSNumber numberWithInt:self.parent.skills.know_geography.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 16:
                if (self.parent.skills.know_history.intValue < totalCharacterLevel) {
                    self.parent.skills.know_history = [NSNumber numberWithInt:self.parent.skills.know_history.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 17:
                if (self.parent.skills.know_local.intValue < totalCharacterLevel) {
                    self.parent.skills.know_local = [NSNumber numberWithInt:self.parent.skills.know_local.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 18:
                if (self.parent.skills.know_nature.intValue < totalCharacterLevel) {
                    self.parent.skills.know_nature = [NSNumber numberWithInt:self.parent.skills.know_nature.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 19:
                if (self.parent.skills.know_nobility.intValue < totalCharacterLevel) {
                    self.parent.skills.know_nobility = [NSNumber numberWithInt:self.parent.skills.know_nobility.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 20:
                if (self.parent.skills.know_planes.intValue < totalCharacterLevel) {
                    self.parent.skills.know_planes = [NSNumber numberWithInt:self.parent.skills.know_planes.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 21:
                if (self.parent.skills.know_religion.intValue < totalCharacterLevel) {
                    self.parent.skills.know_religion = [NSNumber numberWithInt:self.parent.skills.know_religion.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 22:
                if (self.parent.skills.linguistics.intValue < totalCharacterLevel) {
                    self.parent.skills.linguistics = [NSNumber numberWithInt:self.parent.skills.linguistics.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 23:
                if (self.parent.skills.perception.intValue < totalCharacterLevel) {
                    self.parent.skills.perception = [NSNumber numberWithInt:self.parent.skills.perception.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 24:
                if (self.parent.skills.ride.intValue < totalCharacterLevel) {
                    self.parent.skills.ride = [NSNumber numberWithInt:self.parent.skills.ride.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 25:
                if (self.parent.skills.sense.intValue < totalCharacterLevel) {
                    self.parent.skills.sense = [NSNumber numberWithInt:self.parent.skills.sense.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 26:
                if (self.parent.skills.sleight.intValue < totalCharacterLevel) {
                    self.parent.skills.sleight = [NSNumber numberWithInt:self.parent.skills.sleight.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 27:
                if (self.parent.skills.spellcraft.intValue < totalCharacterLevel) {
                    self.parent.skills.spellcraft = [NSNumber numberWithInt:self.parent.skills.spellcraft.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 28:
                if (self.parent.skills.stealth.intValue < totalCharacterLevel) {
                    self.parent.skills.stealth = [NSNumber numberWithInt:self.parent.skills.stealth.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 29:
                if (self.parent.skills.survival.intValue < totalCharacterLevel) {
                    self.parent.skills.survival = [NSNumber numberWithInt:self.parent.skills.survival.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 30:
                if (self.parent.skills.swim.intValue < totalCharacterLevel) {
                    self.parent.skills.swim = [NSNumber numberWithInt:self.parent.skills.swim.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
            case 31:
                if (self.parent.skills.use_magic.intValue < totalCharacterLevel) {
                    self.parent.skills.use_magic = [NSNumber numberWithInt:self.parent.skills.use_magic.intValue + 1];
                    [[self tableView] reloadData];
                    [self setSkillsBadge];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Can't Add Rank" message:@"This skill has the maximum allotted ranks.\nYou must spend all ranks before using skills." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                }
                break;
                
            default:
                break;
        }
        if (skillRanksSpent == totalSkillRanks) {
            UIAlertView * confirm = [[UIAlertView alloc] initWithTitle:@"Save Skill Ranks?" message:@"Would you like to save your changes or reset and start over?" delegate:self cancelButtonTitle:@"Reset" otherButtonTitles:@"Save", nil];
            [confirm show];
        }
    } else {
        int strMod = self.parent.character.str.intValue / 2 - 5;
        int dexMod = self.parent.character.dex.intValue / 2 - 5;
        int intMod = self.parent.character.intel.intValue / 2 - 5;
        int wisMod = self.parent.character.wis.intValue / 2 - 5;
        int chaMod = self.parent.character.cha.intValue / 2 - 5;
        int roll = (arc4random() % 20) + 1;
        int ranks = 0;
        NSNumber * bonus = [skillBonus objectAtIndex:indexPath.row];
        switch (indexPath.row) {
            case 0:
                ranks = self.parent.skills.acrobatics.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Acrobatics Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Dex)", roll + ranks + bonus.intValue + dexMod, roll, ranks, bonus.intValue, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Acrobatics Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Dex)", roll + ranks + dexMod, roll, ranks,  dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Acrobatics Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Dex)", roll + dexMod, roll,  dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 1:
                ranks = self.parent.skills.appraise.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Appraise Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue + intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Appraise Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Appraise Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 2:
                ranks = self.parent.skills.bluff.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Bluff Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Cha)", roll + ranks + bonus.intValue + chaMod, roll, ranks, bonus.intValue, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Bluff Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Cha)", roll + ranks + chaMod, roll, ranks, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Bluff Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Cha)", roll + chaMod, roll, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 3:
                ranks = self.parent.skills.climb.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Climb Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Str)", roll + ranks + bonus.intValue+ strMod, roll, ranks, bonus.intValue, strMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Climb Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Str)", roll + ranks + strMod, roll, ranks, strMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Climb Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Str)", roll +  strMod, roll, strMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 4:
                ranks = self.parent.skills.diplomacy.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Diplomacy Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Cha)", roll + ranks + bonus.intValue + chaMod, roll, ranks, bonus.intValue, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Diplomacy Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Cha)", roll + ranks + chaMod, roll, ranks, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Diplomacy Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Cha)", roll + chaMod, roll, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 5:
                ranks = self.parent.skills.disable.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Disable Device Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Dex)", roll + ranks + bonus.intValue + dexMod, roll, ranks, bonus.intValue, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Disable Device Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Dex)", roll + ranks + dexMod, roll, ranks, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Disable Device Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Dex)", roll + dexMod, roll, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Disable Device in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 6:
                ranks = self.parent.skills.disguise.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Disguise Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Cha)", roll + ranks + bonus.intValue + chaMod, roll, ranks, bonus.intValue, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Disguise Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Cha)", roll + ranks + chaMod, roll, ranks, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Disguise Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Cha)", roll + chaMod, roll, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 7:
                ranks = self.parent.skills.escape.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Escape Artist Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Dex)", roll + ranks + bonus.intValue+ dexMod, roll, ranks, bonus.intValue, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Escape Artist Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Dex)", roll + ranks + dexMod, roll, ranks, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Escape Artist Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Dex)", roll + dexMod, roll, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 8:
                ranks = self.parent.skills.fly.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Fly Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Dex)", roll + ranks + bonus.intValue+ dexMod, roll, ranks, bonus.intValue, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Fly Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Dex)", roll + ranks + dexMod, roll, ranks, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Fly Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Dex)", roll + dexMod, roll, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 9:
                ranks = self.parent.skills.handle.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Handle Animal Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Cha)", roll + ranks + bonus.intValue + chaMod, roll, ranks, bonus.intValue,chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Handle Animal Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Cha)", roll + ranks + chaMod, roll, ranks,chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Handle Animal Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Cha)", roll + chaMod, roll,chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Handle Animal in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 10:
                ranks = self.parent.skills.heal.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Heal Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Wis)", roll + ranks + bonus.intValue+ wisMod, roll, ranks, bonus.intValue, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Heal Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Wis)", roll + ranks + wisMod, roll, ranks, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Heal Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Wis)", roll + wisMod, roll, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 11:
                ranks = self.parent.skills.intimidate.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Intimidate Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Cha)", roll + ranks + bonus.intValue+ chaMod, roll, ranks, bonus.intValue, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Intimidate Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Cha)", roll + ranks + chaMod, roll, ranks, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Intimidate Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Cha)", roll + chaMod, roll, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 12:
                ranks = self.parent.skills.know_arcana.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Arcana) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Arcana) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Arcana) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Arcana) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 13:
                ranks = self.parent.skills.know_dungeon.intValue;
                if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Dungeoneering) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Dungeoneering) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Dungeoneering) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Dungeoneering) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 14:
                ranks = self.parent.skills.know_engine.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Engineering) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Engineering) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Engineering) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Engineering) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 15:
                ranks = self.parent.skills.know_geography.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Geography) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Geography) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Geography) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Geography) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 16:
                ranks = self.parent.skills.know_history.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (History) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (History) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (History) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (History) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 17:
                ranks = self.parent.skills.know_local.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Local) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Local) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Local) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Local) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 18:
                ranks = self.parent.skills.know_nature.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Nature) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Nature) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Nature) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Nature) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 19:
                ranks = self.parent.skills.know_nobility.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Nobility) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Nobility) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Nobility) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Nobility) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 20:
                ranks = self.parent.skills.know_planes.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Planes) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Planes) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Planes) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Planes) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 21:
                ranks = self.parent.skills.know_religion.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Religion) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Religion) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Knowledge (Religion) Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Knowledge (Religion) in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 22:
                ranks = self.parent.skills.linguistics.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Linguistics Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Linguistics Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Linguistics Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Linguistics in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 23:
                ranks = self.parent.skills.perception.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Perception Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Wis)", roll + ranks + bonus.intValue+ wisMod, roll, ranks, bonus.intValue, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Perception Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Wis)", roll + ranks + wisMod, roll, ranks, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Perception Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Wis)", roll + wisMod, roll, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 24:
                ranks = self.parent.skills.ride.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Ride Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Dex)", roll + ranks + bonus.intValue+ dexMod, roll, ranks, bonus.intValue, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Ride Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Dex)", roll + ranks + dexMod, roll, ranks, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Ride Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Dex)", roll + dexMod, roll, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 25:
                ranks = self.parent.skills.sense.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Sense Motive Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Wis)", roll + ranks + bonus.intValue+ wisMod, roll, ranks, bonus.intValue, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Sense Motive Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Wis)", roll + ranks + wisMod, roll, ranks, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Sense Motive Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Wis)", roll + wisMod, roll, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 26:
                ranks = self.parent.skills.sleight.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Sleight of Hand Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Dex)", roll + ranks + bonus.intValue+ dexMod, roll, ranks, bonus.intValue, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Sleight of Hand Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Dex)", roll + ranks + dexMod, roll, ranks, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Sleight of Hand Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Dex)", roll + dexMod, roll, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Sleight of Hand in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 27:
                ranks = self.parent.skills.spellcraft.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Spellcraft Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Int)", roll + ranks + bonus.intValue+ intMod, roll, ranks, bonus.intValue, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Spellcraft Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Int)", roll + ranks + intMod, roll, ranks, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Spellcraft Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Int)", roll + intMod, roll, intMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Spellcraft in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 28:
                ranks = self.parent.skills.stealth.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Stealth Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Dex)", roll + ranks + bonus.intValue+ dexMod, roll, ranks, bonus.intValue, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Stealth Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Dex)", roll + ranks + dexMod, roll, ranks, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Stealth Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Dex)", roll + dexMod, roll, dexMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 29:
                ranks = self.parent.skills.survival.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Survival Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Wis)", roll + ranks + bonus.intValue+ wisMod, roll, ranks, bonus.intValue, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Survival Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Wis)", roll + ranks + wisMod, roll, ranks, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Survival Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Wis)", roll + wisMod, roll, wisMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 30:
                ranks = self.parent.skills.swim.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Swim Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Str)", roll + ranks + bonus.intValue+ strMod, roll, ranks, bonus.intValue, strMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Swim Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Str)", roll + ranks + strMod, roll, ranks, strMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    alert = [[UIAlertView alloc] initWithTitle:@"Swim Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Str)", roll + strMod, roll, strMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
            case 31:
                ranks = self.parent.skills.use_magic.intValue;
                if (ranks > 0 && bonus.intValue > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Use Magic Device Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nClass Bonus: %d\nStat Bonus: %d (Cha)", roll + ranks + bonus.intValue+ chaMod, roll, ranks, bonus.intValue, chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else if (ranks > 0)
                    alert = [[UIAlertView alloc] initWithTitle:@"Use Magic Device Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nRanks: %d\nStat Bonus: %d (Cha)", roll + ranks + chaMod, roll, ranks,  chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                else
                    //alert = [[UIAlertView alloc] initWithTitle:@"Use Magic Device Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nRoll: %d\nStat Bonus: %d (Cha)", roll + chaMod, roll,  chaMod]  delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    alert = [[UIAlertView alloc] initWithTitle:@"Untrained Skill" message:@"You must be trained in Use Magic Device in order to use the skill." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    
                [alert show];
                break;
                
            default:
                break;
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self.parent.context refreshObject:self.parent.skills mergeChanges:FALSE];
            [[self tableView] reloadData];
            [self setSkillsBadge];
            break;
        case 1:
            [self.parent saveContext];
            
        default:
            break;
    }
}

@end
