//
//  SkillData.h
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CharacterData;

@interface SkillData : NSManagedObject

@property (nonatomic, retain) NSNumber * acrobatics;
@property (nonatomic, retain) NSNumber * appraise;
@property (nonatomic, retain) NSNumber * bluff;
@property (nonatomic, retain) NSNumber * climb;
@property (nonatomic, retain) NSNumber * diplomacy;
@property (nonatomic, retain) NSNumber * disable;
@property (nonatomic, retain) NSNumber * disguise;
@property (nonatomic, retain) NSNumber * escape;
@property (nonatomic, retain) NSNumber * fly;
@property (nonatomic, retain) NSNumber * handle;
@property (nonatomic, retain) NSNumber * heal;
@property (nonatomic, retain) NSNumber * intimidate;
@property (nonatomic, retain) NSNumber * know_arcana;
@property (nonatomic, retain) NSNumber * know_dungeon;
@property (nonatomic, retain) NSNumber * know_engine;
@property (nonatomic, retain) NSNumber * know_geography;
@property (nonatomic, retain) NSNumber * know_history;
@property (nonatomic, retain) NSNumber * know_local;
@property (nonatomic, retain) NSNumber * know_nature;
@property (nonatomic, retain) NSNumber * know_nobility;
@property (nonatomic, retain) NSNumber * know_planes;
@property (nonatomic, retain) NSNumber * know_religion;
@property (nonatomic, retain) NSNumber * linguistics;
@property (nonatomic, retain) NSNumber * perception;
@property (nonatomic, retain) NSNumber * ride;
@property (nonatomic, retain) NSNumber * sense;
@property (nonatomic, retain) NSNumber * sleight;
@property (nonatomic, retain) NSNumber * spellcraft;
@property (nonatomic, retain) NSNumber * stealth;
@property (nonatomic, retain) NSNumber * survival;
@property (nonatomic, retain) NSNumber * swim;
@property (nonatomic, retain) NSNumber * use_magic;
@property (nonatomic, retain) CharacterData *character;

@end
