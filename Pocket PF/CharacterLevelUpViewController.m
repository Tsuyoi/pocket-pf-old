//
//  CharacterLevelUpViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 5/1/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "CharacterLevelUpViewController.h"


@implementation CharacterLevelUpViewController {
    int conMod;
    NSString * class;
    int classIndex;
    UITextField * hpField;
    int tempHP;
}

@synthesize parent;
@synthesize classLabel;
@synthesize hitDieLabel;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        class = @"Barbarian";
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)cancelLevelUp:(id)sender {
    [[self navigationController] popViewControllerAnimated:TRUE];
}

- (IBAction)saveLevelUp:(id)sender {
    if (tempHP == 0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Need to Roll Hit Die" message:@"You must roll for hit points before saving changes." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([self.classLabel.text isEqualToString:@"Barbarian"])
        self.parent.classes.barbarian = [NSNumber numberWithInt:self.parent.classes.barbarian.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Bard"])
        self.parent.classes.bard = [NSNumber numberWithInt:self.parent.classes.bard.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Cleric"])
        self.parent.classes.cleric = [NSNumber numberWithInt:self.parent.classes.cleric.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Druid"])
        self.parent.classes.druid = [NSNumber numberWithInt:self.parent.classes.druid.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Fighter"])
        self.parent.classes.fighter = [NSNumber numberWithInt:self.parent.classes.fighter.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Monk"])
        self.parent.classes.monk = [NSNumber numberWithInt:self.parent.classes.monk.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Paladin"])
        self.parent.classes.paladin = [NSNumber numberWithInt:self.parent.classes.paladin.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Ranger"])
        self.parent.classes.ranger = [NSNumber numberWithInt:self.parent.classes.ranger.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Rogue"])
        self.parent.classes.rogue = [NSNumber numberWithInt:self.parent.classes.rogue.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Sorcerer"])
        self.parent.classes.sorcerer = [NSNumber numberWithInt:self.parent.classes.sorcerer.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Wizard"])
        self.parent.classes.wizard = [NSNumber numberWithInt:self.parent.classes.wizard.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Alchemist"])
        self.parent.classes.alchemist = [NSNumber numberWithInt:self.parent.classes.alchemist.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Cavalier"])
        self.parent.classes.cavalier = [NSNumber numberWithInt:self.parent.classes.cavalier.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Gunslinger"])
        self.parent.classes.gunslinger = [NSNumber numberWithInt:self.parent.classes.gunslinger.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Inquisitor"])
        self.parent.classes.inquisitor = [NSNumber numberWithInt:self.parent.classes.inquisitor.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Magus"])
        self.parent.classes.magus = [NSNumber numberWithInt:self.parent.classes.magus.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Oracle"])
        self.parent.classes.oracle = [NSNumber numberWithInt:self.parent.classes.oracle.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Summoner"])
        self.parent.classes.summoner = [NSNumber numberWithInt:self.parent.classes.summoner.intValue + 1];
    if ([self.classLabel.text isEqualToString:@"Witch"])
        self.parent.classes.witch = [NSNumber numberWithInt:self.parent.classes.witch.intValue + 1];
    self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + tempHP + conMod];
    self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.currhp.intValue + tempHP + conMod];
    [[self parent] saveContext];
    [[self navigationController] popViewControllerAnimated:TRUE];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SelectClass"]) {
        ClassPickerViewController * classPicker = segue.destinationViewController;
        classPicker.delegate = self;
        classPicker.className = class;
    }
}

- (void)classPickerViewController:(ClassPickerViewController *)controller didSelectClass:(NSString *)className classIndex:(int)index {
    class = className;
    classIndex = index;
    self.classLabel.text = class;
    tempHP = 0;
    self.hitDieLabel.text = @"0";
    [[self navigationController] popViewControllerAnimated:TRUE];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.classLabel.text = class;
    conMod = (self.parent.character.con.intValue / 2) - 5;
    tempHP = 0;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 3;
            
        default:
            return 0;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    [[self tableView] deselectRowAtIndexPath:indexPath animated:TRUE];
    UIAlertView * alert;
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 2:
                    alert = [[UIAlertView alloc] initWithTitle:@"Add Hit Die" message:@"Enter Hit Die roll for this level\nEnter 0 to randomly roll\nBlah" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
                    [alert setTag:0];
                    hpField = [[UITextField alloc] initWithFrame:CGRectMake(12, 90, 260, 25)];
                    [hpField setBackgroundColor:[UIColor whiteColor]];
                    [hpField setBorderStyle:UITextBorderStyleRoundedRect];
                    [hpField setKeyboardType:UIKeyboardTypeNumberPad];
                    [hpField setTextAlignment:UITextAlignmentRight];
                    if ([self.classLabel.text isEqualToString:@"Barbarian"])
                        [hpField setPlaceholder:@"d12"];
                    if ([self.classLabel.text isEqualToString:@"Bard"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Cleric"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Druid"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Fighter"])
                        [hpField setPlaceholder:@"d10"];
                    if ([self.classLabel.text isEqualToString:@"Monk"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Paladin"])
                        [hpField setPlaceholder:@"d10"];
                    if ([self.classLabel.text isEqualToString:@"Ranger"])
                        [hpField setPlaceholder:@"d10"];
                    if ([self.classLabel.text isEqualToString:@"Rogue"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Sorcerer"])
                        [hpField setPlaceholder:@"d6"];
                    if ([self.classLabel.text isEqualToString:@"Wizard"])
                        [hpField setPlaceholder:@"d6"];
                    if ([self.classLabel.text isEqualToString:@"Alchemist"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Cavalier"])
                        [hpField setPlaceholder:@"d10"];
                    if ([self.classLabel.text isEqualToString:@"Gunslinger"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Inquisitor"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Magus"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Oracle"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Summoner"])
                        [hpField setPlaceholder:@"d8"];
                    if ([self.classLabel.text isEqualToString:@"Witch"])
                        [hpField setPlaceholder:@"d6"];
                    [alert addSubview:hpField];
                    [hpField becomeFirstResponder];
                    [alert show];
                    break;
                    
                default:
                    break;
            }
            break;
            
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch ([alertView tag]) {
        case 0:
            if (buttonIndex == 0) {
                NSString * value = hpField.text;
                int hpRoll = [value intValue];
                /*if ([self.classLabel.text isEqualToString:@"Barbarian"]) {
                    if (hpRoll > 0 && hpRoll < 13) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 12) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Bard"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Cleric"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Druid"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Fighter"]) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Monk"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Paladin"]) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Ranger"]) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Rogue"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Sorcerer"]) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Wizard"]) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Alchemist"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Cavalier"]) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Gunslinger"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Inquisitor"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Magus"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Oracle"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Summoner"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Witch"]) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + hpRoll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + roll];
                        self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.hp.intValue - hpMargin];
                        
                    }
                }
                self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.currhp.intValue + conMod];
                self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + conMod];*/
                if ([self.classLabel.text isEqualToString:@"Barbarian"]) {
                    if (hpRoll > 0 && hpRoll < 13) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 12) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Bard"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Cleric"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Druid"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Fighter"]) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Monk"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Paladin"]) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Ranger"]) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Rogue"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Sorcerer"]) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Wizard"]) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Alchemist"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }                if ([self.classLabel.text isEqualToString:@"Cavalier"]) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Gunslinger"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Inquisitor"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Magus"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Oracle"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Summoner"]) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        tempHP = roll;
                    }
                }
                if ([self.classLabel.text isEqualToString:@"Witch"]) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        tempHP = hpRoll;
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        tempHP = roll;
                    }
                }
                self.hitDieLabel.text = [NSString stringWithFormat:@"%d", tempHP];
            }
            break;
            
        default:
            break;
    }
}

@end
