//
//  Character.h
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Character : NSObject

@property (assign, nonatomic) int alignment;
@property (assign, nonatomic) int deity;
@property (assign, nonatomic) int age;
@property (assign, nonatomic) int height;
@property (assign, nonatomic) int weight;
@property (assign, nonatomic) int race;
@property (assign, nonatomic) int gender;
@property (assign, nonatomic) int cls;

@property (assign, nonatomic) int str;
@property (assign, nonatomic) int dex;
@property (assign, nonatomic) int con;
@property (assign, nonatomic) int intel;
@property (assign, nonatomic) int wis;
@property (assign, nonatomic) int cha;

@property (assign, nonatomic) int gold;
@property (assign, nonatomic) int maxHP;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * owner;
@property (strong, nonatomic) NSString * homeland;
@property (strong, nonatomic) NSString * hair;
@property (strong, nonatomic) NSString * eyes;

@property (strong, nonatomic) NSString * raceLabel;
@property (strong, nonatomic) NSString * classLabel;
@property (strong, nonatomic) NSString * alignmentLabel;

@end
