//
//  SpellDetailsTableViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 4/9/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpellSource.h"

@interface SpellDetailsTableViewController : UITableViewController

@property (nonatomic, strong) SpellSource * spell;

@property (nonatomic, strong) IBOutlet UILabel * lblSpellName;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellSchool;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellLevel;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellCasting;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellComponents;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellRange;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellTarget;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellDuration;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellSave;
@property (nonatomic, strong) IBOutlet UILabel * lblSpellResist;
@property (nonatomic, strong) IBOutlet UITextView * txtSpellDescription;

@end
