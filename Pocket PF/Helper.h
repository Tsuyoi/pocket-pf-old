//
//  Helper.h
//  Pocket PF
//
//  Created by Caylin Hickey on 3/27/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClassData.h"
#import "FeatData.h"
#import "SkillData.h"
#import "SpellData.h"

@interface Helper : NSObject

+ (NSString *)resolveAlignmentToString:(NSNumber *)index shortened:(BOOL)abbv;
+ (NSNumber *)resolveBABFromClass:(ClassData *)index;
+ (NSNumber *)resolveFortSaveFromClass:(ClassData *)index;
+ (NSNumber *)resolveRefSaveFromClass:(ClassData *)index;
+ (NSNumber *)resolveWillSaveFromClass:(ClassData *)index;
+ (UIImage *)resolveClassToImage:(NSNumber *)index;
+ (NSString *)resolveClassToString:(NSNumber *)index;
+ (NSNumber *)resolveClassLevelToSpellLevel:(ClassData *)index class:(NSNumber *)classNumber;
+ (NSNumber *)resolveClassSpellLevelToSpellsKnown:(ClassData *)index ClassID:(NSNumber *)classNumber SpellLevel:(NSNumber *)spellLevel;
+ (NSString *)resolveDietyToString:(NSNumber *)index;
+ (NSString *)resolveGenderToString:(NSNumber *)index;
+ (NSNumber *)resolveLevelToExperience:(NSNumber *)index Progression:(NSNumber *)progression;
+ (NSString *)resolveRaceToString:(NSNumber *)index;
+ (NSString *)resolveRowToSkillString:(NSInteger)index;
+ (NSNumber *)resolveSkillFromSkillData:(SkillData *)index Index:(NSInteger)skill;
@end
