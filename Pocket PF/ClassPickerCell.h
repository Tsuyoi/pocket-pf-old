//
//  ClassPickerCell.h
//  Pocket PF
//
//  Created by Caylin Hickey on 3/29/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassPickerCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * lblClassName;
@property (nonatomic, strong) IBOutlet UIImageView * imgClassImage;

@end
