//
//  CharacterLevelUpViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 5/1/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CharacterInfoTabBarController.h"
#import "ClassPickerViewController.h"

@interface CharacterLevelUpViewController : UITableViewController <ClassPickerViewControllerDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) CharacterInfoTabBarController * parent;
@property (nonatomic, strong) IBOutlet UILabel * classLabel;
@property (nonatomic, strong) IBOutlet UILabel * hitDieLabel;

- (IBAction)cancelLevelUp:(id)sender;
- (IBAction)saveLevelUp:(id)sender;

@end
