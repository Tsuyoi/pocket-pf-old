//
//  ClassData.h
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CharacterData;

@interface ClassData : NSManagedObject

@property (nonatomic, retain) NSNumber * alchemist;
@property (nonatomic, retain) NSNumber * barbarian;
@property (nonatomic, retain) NSNumber * bard;
@property (nonatomic, retain) NSNumber * cavalier;
@property (nonatomic, retain) NSNumber * cleric;
@property (nonatomic, retain) NSNumber * druid;
@property (nonatomic, retain) NSNumber * fighter;
@property (nonatomic, retain) NSNumber * gunslinger;
@property (nonatomic, retain) NSNumber * inquisitor;
@property (nonatomic, retain) NSNumber * magus;
@property (nonatomic, retain) NSNumber * monk;
@property (nonatomic, retain) NSNumber * oracle;
@property (nonatomic, retain) NSNumber * paladin;
@property (nonatomic, retain) NSNumber * ranger;
@property (nonatomic, retain) NSNumber * rogue;
@property (nonatomic, retain) NSNumber * sorcerer;
@property (nonatomic, retain) NSNumber * summoner;
@property (nonatomic, retain) NSNumber * witch;
@property (nonatomic, retain) NSNumber * wizard;
@property (nonatomic, retain) CharacterData *character;

@end
