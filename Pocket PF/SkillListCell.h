//
//  SkillListCell.h
//  Pocket PF
//
//  Created by Caylin Hickey on 3/31/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkillListCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * skillName;
@property (nonatomic, strong) IBOutlet UILabel * skillTotal;

@end
