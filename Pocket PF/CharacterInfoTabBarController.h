//
//  CharacterInfoTabBarController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 3/30/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CharacterData.h"
#import "ClassData.h"
#import "FeatData.h"
#import "SpellData.h"
#import "SkillData.h"

@interface CharacterInfoTabBarController : UITabBarController <UIAlertViewDelegate>

@property (nonatomic, strong) CharacterData * character;
@property (nonatomic, strong) ClassData * classes;
@property (nonatomic, strong) NSSet * feats;
@property (nonatomic, strong) NSSet * spells;
@property (nonatomic, strong) SkillData * skills;
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) IBOutlet UIBarButtonItem * levelUpButton;

- (void)saveContext;
- (void)refreshSkillsBadge;

@end
