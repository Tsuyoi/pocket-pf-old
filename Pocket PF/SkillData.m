//
//  SkillData.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "SkillData.h"
#import "CharacterData.h"


@implementation SkillData

@dynamic acrobatics;
@dynamic appraise;
@dynamic bluff;
@dynamic climb;
@dynamic diplomacy;
@dynamic disable;
@dynamic disguise;
@dynamic escape;
@dynamic fly;
@dynamic handle;
@dynamic heal;
@dynamic intimidate;
@dynamic know_arcana;
@dynamic know_dungeon;
@dynamic know_engine;
@dynamic know_geography;
@dynamic know_history;
@dynamic know_local;
@dynamic know_nature;
@dynamic know_nobility;
@dynamic know_planes;
@dynamic know_religion;
@dynamic linguistics;
@dynamic perception;
@dynamic ride;
@dynamic sense;
@dynamic sleight;
@dynamic spellcraft;
@dynamic stealth;
@dynamic survival;
@dynamic swim;
@dynamic use_magic;
@dynamic character;

@end
