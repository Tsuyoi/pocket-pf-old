//
//  WeaponSource.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "WeaponSource.h"


@implementation WeaponSource

@dynamic name;
@dynamic cost;
@dynamic dmgsdice;
@dynamic dmgs;
@dynamic dmgmdice;
@dynamic dmgm;
@dynamic critlow;
@dynamic crithigh;
@dynamic critmulti;
@dynamic range;
@dynamic weight;
@dynamic type;
@dynamic special;
@dynamic cls;
@dynamic category;

@end
