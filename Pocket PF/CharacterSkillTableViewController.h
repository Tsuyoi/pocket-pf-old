//
//  CharacterSkillTableViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 3/31/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CharacterInfoTabBarController.h"

@interface CharacterSkillTableViewController : UITableViewController <UIAlertViewDelegate>

@property (nonatomic, strong) CharacterInfoTabBarController * parent;
@property (assign, nonatomic) NSInteger charIndex;

@end
