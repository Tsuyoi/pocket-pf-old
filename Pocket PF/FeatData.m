//
//  FeatData.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "FeatData.h"
#import "CharacterData.h"


@implementation FeatData

@dynamic name;
@dynamic prereq;
@dynamic summary;
@dynamic type;
@dynamic character;

@end
