//
//  CharacterBasicInfoEditViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 3/31/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CharacterInfoTabBarController.h"
#import "DeityPickerViewController.h"

@interface CharacterBasicInfoEditViewController : UITableViewController <DeityPickerViewControllerDelegate>

@property (nonatomic, strong) CharacterInfoTabBarController * parent;

@property (nonatomic, strong) IBOutlet UITextField * txtName;
@property (nonatomic, strong) IBOutlet UITextField * txtAge;
@property (nonatomic, strong) IBOutlet UITextField * txtHeight;
@property (nonatomic, strong) IBOutlet UITextField * txtWeight;
@property (nonatomic, strong) IBOutlet UITextField * txtHair;
@property (nonatomic, strong) IBOutlet UITextField * txtEyes;
@property (nonatomic, strong) IBOutlet UITextField * txtHomeland;
@property (nonatomic, strong) IBOutlet UILabel * lblDeity;

- (IBAction)saveChanges;

@end
