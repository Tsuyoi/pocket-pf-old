//
//  CharacterBasicInfoEditViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 3/31/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "CharacterBasicInfoEditViewController.h"
#import "Helper.h"

@implementation CharacterBasicInfoEditViewController

@synthesize parent;

@synthesize txtName;
@synthesize txtAge;
@synthesize txtHeight;
@synthesize txtWeight;
@synthesize txtHair;
@synthesize txtEyes;
@synthesize txtHomeland;
@synthesize lblDeity;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.txtName.text = self.parent.character.name;
    if (self.parent.character.age.intValue != 0)
        self.txtAge.text = [NSString stringWithFormat:@"%@", self.parent.character.age];
    if (self.parent.character.height.intValue != 0)
        self.txtHeight.text = [NSString stringWithFormat:@"%@", self.parent.character.height];
    if (self.parent.character.weight.intValue != 0)
        self.txtWeight.text = [NSString stringWithFormat:@"%@", self.parent.character.weight];
    self.txtHair.text = self.parent.character.hair;
    self.txtEyes.text = self.parent.character.eyes;
    self.txtHomeland.text = self.parent.character.homeland;
    self.lblDeity.text = [Helper resolveDietyToString:self.parent.character.deity];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 8;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SelectDeity"]) {
        DeityPickerViewController * deityPicker = segue.destinationViewController;
        deityPicker.delegate = self;
        deityPicker.deity = self.parent.character.deity;
    }
}

- (void)deityPickerViewController:(DeityPickerViewController *)controller didSelectDeity:(NSNumber *)deityIndex {
    self.parent.character.deity = deityIndex;
    self.lblDeity.text = [Helper resolveDietyToString:self.parent.character.deity];
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)didFinishEditing:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)saveChanges {
    self.parent.character.name = self.txtName.text;
    if (self.txtAge.text.length > 0)
        self.parent.character.age = [NSNumber numberWithInt:self.txtAge.text.intValue];
    if (self.txtHeight.text.length > 0)
        self.parent.character.height = [NSNumber numberWithInt:self.txtHeight.text.intValue];
    if (self.txtWeight.text.length > 0)
        self.parent.character.weight = [NSNumber numberWithInt:self.txtWeight.text.intValue];
    if (self.txtHair.text.length > 0)
        self.parent.character.hair = self.txtHair.text;
    if (self.txtEyes.text.length > 0)
        self.parent.character.eyes = self.txtEyes.text;
    if (self.txtHomeland.text.length > 0)
        self.parent.character.homeland = self.txtHomeland.text;
    [[self parent] saveContext];
    [self.navigationController popViewControllerAnimated:TRUE];
}

@end
