//
//  SpellDetailsTableViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 4/9/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "SpellDetailsTableViewController.h"


@implementation SpellDetailsTableViewController

@synthesize spell;

@synthesize txtSpellDescription;
@synthesize lblSpellComponents;
@synthesize lblSpellCasting;
@synthesize lblSpellSave;
@synthesize lblSpellSchool;
@synthesize lblSpellResist;
@synthesize lblSpellLevel;
@synthesize lblSpellDuration;
@synthesize lblSpellName;
@synthesize lblSpellRange;
@synthesize lblSpellTarget;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    lblSpellName.text = spell.name;
    lblSpellSchool.text = spell.school;
    lblSpellLevel.text = spell.spell_level;
    lblSpellCasting.text = spell.casting_time;
    lblSpellComponents.text = spell.components;
    if (spell.components.length < 25) {
        CGRect compFrame = lblSpellComponents.frame;
        compFrame.size = CGSizeMake(182, 21);
        lblSpellComponents.frame = compFrame;
        lblSpellComponents.numberOfLines = 1;
    } else if (spell.components.length < 50) {
        CGRect compFrame = lblSpellComponents.frame;
        compFrame.size = CGSizeMake(182, 42);
        lblSpellComponents.frame = compFrame;
        lblSpellComponents.numberOfLines = 2;
    } else {
        CGRect compFrame = lblSpellComponents.frame;
        compFrame.size = CGSizeMake(182, 63);
        lblSpellComponents.frame = compFrame;
        lblSpellComponents.numberOfLines = 3;
    }
    lblSpellRange.text = spell.range;
    if (spell.range.length < 40) {
        CGRect compFrame = lblSpellRange.frame;
        compFrame.size = CGSizeMake(206, 21);
        lblSpellRange.frame = compFrame;
        lblSpellRange.numberOfLines = 1;
    } else {
        CGRect compFrame = lblSpellRange.frame;
        compFrame.size = CGSizeMake(206, 42);
        lblSpellRange.frame = compFrame;
        lblSpellRange.numberOfLines = 2;
    }
    lblSpellTarget.text = spell.targets;
    if (spell.targets.length < 40) {
        CGRect compFrame = lblSpellTarget.frame;
        compFrame.size = CGSizeMake(206, 21);
        lblSpellTarget.frame = compFrame;
        lblSpellTarget.numberOfLines = 1;
    } else {
        CGRect compFrame = lblSpellTarget.frame;
        compFrame.size = CGSizeMake(206, 42);
        lblSpellTarget.frame = compFrame;
        lblSpellTarget.numberOfLines = 2;
    }
    lblSpellDuration.text = spell.duration;
    if (spell.duration.length < 40) {
        CGRect compFrame = lblSpellDuration.frame;
        compFrame.size = CGSizeMake(206, 21);
        lblSpellDuration.frame = compFrame;
        lblSpellDuration.numberOfLines = 1;
    } else {
        CGRect compFrame = lblSpellDuration.frame;
        compFrame.size = CGSizeMake(206, 42);
        lblSpellDuration.frame = compFrame;
        lblSpellDuration.numberOfLines = 2;
    }
    lblSpellSave.text = spell.saving_throw;
    lblSpellResist.text = spell.spell_resist;
    txtSpellDescription.text = spell.desc;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
