//
//  WeaponData.h
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CharacterData;

@interface WeaponData : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * cost;
@property (nonatomic, retain) NSNumber * dmgsdice;
@property (nonatomic, retain) NSNumber * dmgs;
@property (nonatomic, retain) NSNumber * dmgmdice;
@property (nonatomic, retain) NSNumber * dmgm;
@property (nonatomic, retain) NSNumber * critlow;
@property (nonatomic, retain) NSNumber * crithigh;
@property (nonatomic, retain) NSNumber * critmulti;
@property (nonatomic, retain) NSNumber * range;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * special;
@property (nonatomic, retain) NSString * cls;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) CharacterData *character;

@end
