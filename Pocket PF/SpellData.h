//
//  SpellData.h
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CharacterData;

@interface SpellData : NSManagedObject

@property (nonatomic, retain) NSNumber * alchemist;
@property (nonatomic, retain) NSString * area;
@property (nonatomic, retain) NSNumber * bard;
@property (nonatomic, retain) NSString * casting_time;
@property (nonatomic, retain) NSNumber * cleric;
@property (nonatomic, retain) NSString * components;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSNumber * dismissible;
@property (nonatomic, retain) NSString * domain;
@property (nonatomic, retain) NSNumber * druid;
@property (nonatomic, retain) NSString * duration;
@property (nonatomic, retain) NSString * effect;
@property (nonatomic, retain) NSNumber * inquisitor;
@property (nonatomic, retain) NSNumber * magus;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * oracle;
@property (nonatomic, retain) NSNumber * paladin;
@property (nonatomic, retain) NSString * range;
@property (nonatomic, retain) NSNumber * ranger;
@property (nonatomic, retain) NSString * saving_throw;
@property (nonatomic, retain) NSString * school;
@property (nonatomic, retain) NSString * short_desc;
@property (nonatomic, retain) NSNumber * sor;
@property (nonatomic, retain) NSString * spell_level;
@property (nonatomic, retain) NSString * spell_resist;
@property (nonatomic, retain) NSString * subschool;
@property (nonatomic, retain) NSNumber * summoner;
@property (nonatomic, retain) NSString * targets;
@property (nonatomic, retain) NSNumber * witch;
@property (nonatomic, retain) NSNumber * wiz;
@property (nonatomic, retain) CharacterData *character;

@end
