//
//  Character.m
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "Character.h"

@implementation Character

@synthesize alignment;
@synthesize deity;
@synthesize age;
@synthesize height;
@synthesize weight;
@synthesize race;
@synthesize gender;
@synthesize cls;

@synthesize str;
@synthesize dex;
@synthesize con;
@synthesize intel;
@synthesize wis;
@synthesize cha;

@synthesize gold;
@synthesize maxHP;
@synthesize name;
@synthesize owner;
@synthesize homeland;
@synthesize hair;
@synthesize eyes;

@synthesize raceLabel;
@synthesize classLabel;
@synthesize alignmentLabel;

@end
