//
//  MagicItemSource.h
//  Pocket PF
//
//  Created by Caylin Hickey on 4/12/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MagicItemSource : NSManagedObject

@property (nonatomic, retain) NSNumber * abjuration;
@property (nonatomic, retain) NSString * aura;
@property (nonatomic, retain) NSString * aura_strength;
@property (nonatomic, retain) NSNumber * cl;
@property (nonatomic, retain) NSNumber * conjuration;
@property (nonatomic, retain) NSString * cost;
@property (nonatomic, retain) NSNumber * cost_value;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSNumber * divination;
@property (nonatomic, retain) NSNumber * enchantment;
@property (nonatomic, retain) NSNumber * evocation;
@property (nonatomic, retain) NSString * group;
@property (nonatomic, retain) NSNumber * majorartifactflag;
@property (nonatomic, retain) NSNumber * minorartifactflag;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * necromancy;
@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSNumber * price_value;
@property (nonatomic, retain) NSString * requirements;
@property (nonatomic, retain) NSString * slot;
@property (nonatomic, retain) NSNumber * transmutation;
@property (nonatomic, retain) NSNumber * weight_value;

@end
