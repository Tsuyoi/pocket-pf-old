//
//  AbilityPickerViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 2/13/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AbilityPickerViewController;

@protocol AbilityPickerViewControllerDelegate <NSObject>

- (void)abilityPickerViewController:(AbilityPickerViewController *)controller didSelectAbility:(NSString *)abilityName;

@end

@interface AbilityPickerViewController : UITableViewController

@property (weak, nonatomic) id <AbilityPickerViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString * ability;

@end
