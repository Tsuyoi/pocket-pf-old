//
//  CharacterBasicInfoTabViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 3/29/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "CharacterBasicInfoEditViewController.h"
#import "CharacterBasicInfoTabViewController.h"
#import "Helper.h"

@implementation CharacterBasicInfoTabViewController {
    int strMod, dexMod, conMod, intMod, wisMod, chaMod;
    int armorClass, armorClassFlat, armorClassTouch;
    int fortSave, refSave, willSave;
    int bab, cmb, cmd;
    NSNumber * expNeeded;
    UITextField * expField;
}

@synthesize parent;

@synthesize lblName;
@synthesize lblRace;
@synthesize lblSize;
@synthesize lblLook;
@synthesize lblCulture;

@synthesize infoImage;

@synthesize expProgress;
@synthesize expValues;

@synthesize hpProgress;
@synthesize hpValues;

@synthesize levelBarbarian;
@synthesize levelBard;
@synthesize levelCleric;
@synthesize levelDruid;
@synthesize levelFighter;
@synthesize levelMonk;
@synthesize levelPaladin;
@synthesize levelRanger;
@synthesize levelRogue;
@synthesize levelSorcerer;
@synthesize levelWizard;
@synthesize levelAlchemist;
@synthesize levelCavalier;
@synthesize levelGunslinger;
@synthesize levelInquisitor;
@synthesize levelMagus;
@synthesize levelOracle;
@synthesize levelSummoner;
@synthesize levelWitch;

@synthesize scoreStr;
@synthesize scoreDex;
@synthesize scoreCon;
@synthesize scoreInt;
@synthesize scoreWis;
@synthesize scoreCha;

@synthesize defAC;
@synthesize defFort;
@synthesize defRef;
@synthesize defWill;
@synthesize defSpellRes;

@synthesize offBAB;
@synthesize offCMB;
@synthesize offCMD;
@synthesize offInit;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.lblName.text = self.parent.character.name;
    NSString * age;
    if (self.parent.character.age.intValue > 0)
        age = [NSString stringWithFormat:@"%@ years old", self.parent.character.age];
    else
        age = @"Age Unknown";
    self.lblRace.text = [NSString stringWithFormat:@"%@ %@, %@", [Helper resolveGenderToString:self.parent.character.female], [Helper resolveRaceToString:self.parent.character.race], age];
    NSString * height;
    NSString * weight;
    if (self.parent.character.height.intValue > 0)
        height = [NSString stringWithFormat:@"%d'%d\"", self.parent.character.height.intValue / 12, self.parent.character.height.intValue % 12];
    else
        height = @"Unknown";
    if (self.parent.character.weight.intValue > 0)
        weight = [NSString stringWithFormat:@"%d lbs", self.parent.character.weight.intValue];
    else
        weight = @"Unknown";
    self.lblSize.text = [NSString stringWithFormat:@"Height: %@ Weight: %@", height, weight];
    self.lblLook.text = [NSString stringWithFormat:@"Hair: %@ Eyes: %@", self.parent.character.hair, self.parent.character.eyes];
    self.lblCulture.text = [NSString stringWithFormat:@"Deity: %@ Homeland: %@", [Helper resolveDietyToString:self.parent.character.deity], self.parent.character.homeland];
    
    self.infoImage.image = [Helper resolveClassToImage:self.parent.character.cls];
    
    self.levelBarbarian.text = [NSString stringWithFormat:@"%@", self.parent.classes.barbarian];
    self.levelBard.text = [NSString stringWithFormat:@"%@", self.parent.classes.bard];
    self.levelCleric.text = [NSString stringWithFormat:@"%@", self.parent.classes.cleric];
    self.levelDruid.text = [NSString stringWithFormat:@"%@", self.parent.classes.druid];
    self.levelFighter.text = [NSString stringWithFormat:@"%@", self.parent.classes.fighter];
    
    self.levelMonk.text = [NSString stringWithFormat:@"%@", self.parent.classes.monk];
    self.levelPaladin.text = [NSString stringWithFormat:@"%@", self.parent.classes.paladin];
    self.levelRanger.text = [NSString stringWithFormat:@"%@", self.parent.classes.ranger];
    self.levelRogue.text = [NSString stringWithFormat:@"%@", self.parent.classes.rogue];
    self.levelSorcerer.text = [NSString stringWithFormat:@"%@", self.parent.classes.sorcerer];
    
    self.levelWizard.text = [NSString stringWithFormat:@"%@", self.parent.classes.wizard];
    
    self.levelAlchemist.text = [NSString stringWithFormat:@"%@", self.parent.classes.alchemist];
    self.levelCavalier.text = [NSString stringWithFormat:@"%@", self.parent.classes.cavalier];
    self.levelGunslinger.text = [NSString stringWithFormat:@"%@", self.parent.classes.gunslinger];
    self.levelInquisitor.text = [NSString stringWithFormat:@"%@", self.parent.classes.inquisitor];
    self.levelMagus.text = [NSString stringWithFormat:@"%@", self.parent.classes.magus];
    
    self.levelOracle.text = [NSString stringWithFormat:@"%@", self.parent.classes.oracle];
    self.levelSummoner.text = [NSString stringWithFormat:@"%@", self.parent.classes.summoner];
    self.levelWitch.text = [NSString stringWithFormat:@"%@", self.parent.classes.witch];
    
    int totalLevel = self.parent.classes.barbarian.intValue + self.parent.classes.bard.intValue + self.parent.classes.cleric.intValue +
    self.parent.classes.druid.intValue + self.parent.classes.fighter.intValue + self.parent.classes.monk.intValue + self.parent.classes.paladin.intValue +
    self.parent.classes.ranger.intValue + self.parent.classes.rogue.intValue + self.parent.classes.sorcerer.intValue + self.parent.classes.wizard.intValue +
    self.parent.classes.alchemist.intValue + self.parent.classes.cavalier.intValue + self.parent.classes.gunslinger.intValue + self.parent.classes.inquisitor.intValue +
    self.parent.classes.magus.intValue + self.parent.classes.oracle.intValue + self.parent.classes.summoner.intValue + self.parent.classes.witch.intValue;
    
    expNeeded = [Helper resolveLevelToExperience:[NSNumber numberWithInt:totalLevel] Progression:[NSNumber numberWithInt:0]];
    
    self.expValues.text = [NSString stringWithFormat:@"%@ / %@", self.parent.character.exp, expNeeded];
    CGFloat progress = (self.parent.character.exp.floatValue / expNeeded.floatValue);
    if (progress > 1.0)
        progress = 1.0;
    self.expProgress.progress = progress;
    if (self.expProgress.progress == 1.0)
        self.parent.levelUpButton.enabled = TRUE;
    else
        self.parent.levelUpButton.enabled = FALSE;
    
    strMod = (self.parent.character.str.intValue / 2) - 5;
    dexMod = (self.parent.character.dex.intValue / 2) - 5;
    conMod = (self.parent.character.con.intValue / 2) - 5;
    intMod = (self.parent.character.intel.intValue / 2) - 5;
    wisMod = (self.parent.character.wis.intValue / 2) - 5;
    chaMod = (self.parent.character.cha.intValue / 2) - 5;
    
    self.scoreStr.text = [NSString stringWithFormat:@"%@ (%d Modifier)", self.parent.character.str, strMod];
    self.scoreDex.text = [NSString stringWithFormat:@"%@ (%d Modifier)", self.parent.character.dex, dexMod];
    self.scoreCon.text = [NSString stringWithFormat:@"%@ (%d Modifier)", self.parent.character.con, conMod];
    self.scoreInt.text = [NSString stringWithFormat:@"%@ (%d Modifier)", self.parent.character.intel, intMod];
    self.scoreWis.text = [NSString stringWithFormat:@"%@ (%d Modifier)", self.parent.character.wis, wisMod];
    self.scoreCha.text = [NSString stringWithFormat:@"%@ (%d Modifier)", self.parent.character.cha, chaMod];
    
    armorClass = dexMod + 10;
    armorClassFlat = 10;
    armorClassTouch = dexMod + 10;
    fortSave = [[Helper resolveFortSaveFromClass:self.parent.classes] intValue] + conMod;
    refSave = [[Helper resolveRefSaveFromClass:self.parent.classes] intValue] + dexMod;
    willSave = [[Helper resolveWillSaveFromClass:self.parent.classes] intValue] + wisMod;
    bab = [[Helper resolveBABFromClass:self.parent.classes] intValue];
    cmb = bab + strMod;
    cmd = bab + strMod + dexMod + 10;
    
    if (self.parent.character.race == [NSNumber numberWithInt:2] || self.parent.character.race == [NSNumber numberWithInt:4]) {
        armorClass++;
        bab++;
        cmb--;
        cmd--;
    }
    
    self.defAC.text = [NSString stringWithFormat:@"%d (%d Touch / %d Flat)", armorClass, armorClassTouch, armorClassFlat];
    self.defFort.text = [NSString stringWithFormat:@"%d", fortSave];
    self.defRef.text = [NSString stringWithFormat:@"%d", refSave];
    self.defWill.text = [NSString stringWithFormat:@"%d", willSave];
    self.defSpellRes.text = @"0";
    
    self.offBAB.text = [NSString stringWithFormat:@"%d", bab];
    self.offCMB.text = [NSString stringWithFormat:@"%d", cmb];
    self.offCMD.text = [NSString stringWithFormat:@"%d", cmd];
    self.offInit.text = [NSString stringWithFormat:@"%d", dexMod];
    if (self.parent.character.hp.intValue == 0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Add Hit Die" message:@"Enter Hit Die roll for this level\nEnter 0 to randomly roll\nBlah" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
        [alert setTag:1];
        expField = [[UITextField alloc] initWithFrame:CGRectMake(12, 90, 260, 25)];
        [expField setBackgroundColor:[UIColor whiteColor]];
        [expField setBorderStyle:UITextBorderStyleRoundedRect];
        [expField setKeyboardType:UIKeyboardTypeNumberPad];
        [expField setTextAlignment:UITextAlignmentRight];
        if (self.parent.classes.barbarian.intValue > 0)
            [expField setPlaceholder:@"d12"];
        if (self.parent.classes.bard.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.cleric.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.druid.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.fighter.intValue > 0)
            [expField setPlaceholder:@"d10"];
        if (self.parent.classes.monk.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.paladin.intValue > 0)
            [expField setPlaceholder:@"d10"];
        if (self.parent.classes.ranger.intValue > 0)
            [expField setPlaceholder:@"d10"];
        if (self.parent.classes.rogue.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.sorcerer.intValue > 0)
            [expField setPlaceholder:@"d6"];
        if (self.parent.classes.wizard.intValue > 0)
            [expField setPlaceholder:@"d6"];
        if (self.parent.classes.alchemist.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.cavalier.intValue > 0)
            [expField setPlaceholder:@"d10"];
        if (self.parent.classes.inquisitor.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.magus.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.oracle.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.summoner.intValue > 0)
            [expField setPlaceholder:@"d8"];
        if (self.parent.classes.witch.intValue > 0)
            [expField setPlaceholder:@"d6"];
        [alert addSubview:expField];
        [expField becomeFirstResponder];
        [alert show];
    }
    
    self.hpValues.text = [NSString stringWithFormat:@"%@ / %@", self.parent.character.currhp, self.parent.character.hp];
    CGFloat hpCurrProgress = (self.parent.character.currhp.floatValue / self.parent.character.hp.floatValue);
    if (hpCurrProgress > 1.0)
        hpCurrProgress = 1.0;
    self.hpProgress.progress = hpCurrProgress;
    
    [self.tableView reloadData];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, 6)] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:0,1,2,3,4,nil] withRowAnimation:UITableViewRowAnimationFade];
    [[self parent] refreshSkillsBadge];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
        case 1:
            return 1;
        case 2:
            return 1;
        case 3:
            return 21;
        case 4:
            return 7;
        case 5:
            return 6;
        case 6:
            return 5;
            
        default:
            return 0;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 3:
            switch (indexPath.row) {
                case 1:
                    if (self.parent.classes.barbarian.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 2:
                    if (self.parent.classes.bard.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 3:
                    if (self.parent.classes.cleric.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 4:
                    if (self.parent.classes.druid.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 5:
                    if (self.parent.classes.fighter.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 6:
                    if (self.parent.classes.monk.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 7:
                    if (self.parent.classes.paladin.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 8:
                    if (self.parent.classes.ranger.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 9:
                    if (self.parent.classes.rogue.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 10:
                    if (self.parent.classes.sorcerer.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 11:
                    if (self.parent.classes.wizard.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;;
                case 12:
                    if (self.parent.classes.alchemist.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 13:
                    if (self.parent.classes.cavalier.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 14:
                    if (self.parent.classes.gunslinger.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 15:
                    if (self.parent.classes.inquisitor.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 16:
                    if (self.parent.classes.magus.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 17:
                    if (self.parent.classes.oracle.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 18:
                    if (self.parent.classes.summoner.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                case 19:
                    if (self.parent.classes.witch.intValue > 0)
                        [cell setHidden:FALSE];
                    else
                        [cell setHidden:TRUE];
                    break;
                    
                default:
                    break;
            }
            
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 160.0;
        case 1:
            return 50.0;
        case 2:
            return 50.0;
        case 3:
            switch (indexPath.row) {
                case 0:
                    return 30.0;
                case 1:
                    if (self.parent.classes.barbarian.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 2:
                    if (self.parent.classes.bard.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 3:
                    if (self.parent.classes.cleric.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 4:
                    if (self.parent.classes.druid.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 5:
                    if (self.parent.classes.fighter.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 6:
                    if (self.parent.classes.monk.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 7:
                    if (self.parent.classes.paladin.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 8:
                    if (self.parent.classes.ranger.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 9:
                    if (self.parent.classes.rogue.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 10:
                    if (self.parent.classes.sorcerer.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 11:
                    if (self.parent.classes.wizard.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 12:
                    if (self.parent.classes.alchemist.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 13:
                    if (self.parent.classes.cavalier.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 14:
                    if (self.parent.classes.gunslinger.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 15:
                    if (self.parent.classes.inquisitor.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 16:
                    if (self.parent.classes.magus.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 17:
                    if (self.parent.classes.oracle.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 18:
                    if (self.parent.classes.summoner.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 19:
                    if (self.parent.classes.witch.intValue > 0)
                        return 41.0;
                    else
                        return 0.0;
                case 20:
                    return 10.0;
                    
                default:
                    return 44.0;
            }
        case 4:
            switch (indexPath.row) {
                case 0:
                    return 30.0;
                    
                default:
                    return 41.0;
            }
        case 5:
            switch (indexPath.row) {
                case 0:
                    return 30.0;
                    
                default:
                    return 41.0;
            }
        case 6:
            switch (indexPath.row) {
                case 0:
                    return 30.0;
                    
                default:
                    return 41.0;
            }
            
        default:
            return 44.0;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EditBasicInfo"]) {
        CharacterBasicInfoEditViewController * editViewController = segue.destinationViewController;
        editViewController.parent = self.parent;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    int roll = (arc4random() % 20) + 1;
    UIAlertView * alert;
    switch (indexPath.section) {
        case 1:
            alert = [[UIAlertView alloc] initWithTitle:@"Gained Experience" message:@"Amount of Experience to Add\n\nBlah" delegate:self cancelButtonTitle:@"Not Now" otherButtonTitles:@"Add", nil];
            [alert setTag:0];
            expField = [[UITextField alloc] initWithFrame:CGRectMake(12, 80, 260, 25)];
            [expField setBackgroundColor:[UIColor whiteColor]];
            [expField setBorderStyle:UITextBorderStyleRoundedRect];
            [expField setKeyboardType:UIKeyboardTypeNumberPad];
            [expField setTextAlignment:UITextAlignmentRight];
            [expField setPlaceholder:@"100"];
            [alert addSubview:expField];
            [expField becomeFirstResponder];
            [alert show];
            break;
        case 2:
            alert = [[UIAlertView alloc] initWithTitle:@"Health Status Change" message:@"Amount of Health to Gain/Lose\n\nBlah" delegate:self cancelButtonTitle:@"Lost" otherButtonTitles:@"Gained", nil];
            [alert setTag:2];
            expField = [[UITextField alloc] initWithFrame:CGRectMake(12, 80, 260, 25)];
            [expField setBackgroundColor:[UIColor whiteColor]];
            [expField setBorderStyle:UITextBorderStyleRoundedRect];
            [expField setKeyboardType:UIKeyboardTypeNumberPad];
            [expField setTextAlignment:UITextAlignmentRight];
            [expField setPlaceholder:@"Enter 0 to Cancel"];
            [alert addSubview:expField];
            [expField becomeFirstResponder];
            [alert show];
            break;
        case 4:
            switch (indexPath.row) {
                case 1:
                    alert = [[UIAlertView alloc] initWithTitle:@"Strength Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + strMod, roll, strMod] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                case 2:
                    alert = [[UIAlertView alloc] initWithTitle:@"Dexterity Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + dexMod, roll, dexMod] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                case 3:
                    alert = [[UIAlertView alloc] initWithTitle:@"Constitution Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + conMod, roll, conMod] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                case 4:
                    alert = [[UIAlertView alloc] initWithTitle:@"Intelligence Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + intMod, roll, intMod] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                case 5:
                    alert = [[UIAlertView alloc] initWithTitle:@"Wisdom Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + wisMod, roll, wisMod] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                case 6:
                    alert = [[UIAlertView alloc] initWithTitle:@"Charisma Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + chaMod, roll, chaMod] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                    
                default:
                    break;
            }
            break;
        case 5:
            switch (indexPath.row) {
                case 2:
                    alert = [[UIAlertView alloc] initWithTitle:@"Fortitude Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + fortSave, roll, fortSave] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                case 3:
                    alert = [[UIAlertView alloc] initWithTitle:@"Reflex Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + refSave, roll, refSave] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                case 4:
                    alert = [[UIAlertView alloc] initWithTitle:@"Will Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + willSave, roll, willSave] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                    
                default:
                    break;
            }
            break;
        case 6:
            switch (indexPath.row) {
                case 2:
                    alert = [[UIAlertView alloc] initWithTitle:@"Combat Maneuver Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + cmb, roll, cmb] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                case 4:
                    alert = [[UIAlertView alloc] initWithTitle:@"Initiative Roll" message:[NSString stringWithFormat:@"Total Roll: %d\nDice Roll: %d\nModifier: %d", roll + dexMod, roll, dexMod] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    break;
                    
                default:
                    break;
            }
            break;
            
        default:
            break;
    }
}

- (void) alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch ([actionSheet tag]) {
        case 0:
            if (buttonIndex == 1) {
                NSString * value = expField.text;
                int expAdded = [value intValue];
                self.parent.character.exp = [NSNumber numberWithInt:self.parent.character.exp.intValue + expAdded];
                self.expValues.text = [NSString stringWithFormat:@"%@ / %@", self.parent.character.exp, expNeeded];
                CGFloat progress = (self.parent.character.exp.floatValue / expNeeded.floatValue);
                if (progress > 1.0)
                    progress = 1.0;
                self.expProgress.progress = progress;
                if (self.expProgress.progress == 1.0)
                    self.parent.levelUpButton.enabled = TRUE;
                [[self parent] saveContext];
            }
            break;
        case 1:
            if (buttonIndex == 0) {
                NSString * value = expField.text;
                int hpRoll = [value intValue];
                if (self.parent.classes.barbarian.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 13) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 12) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.bard.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.cleric.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.druid.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.fighter.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.monk.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.paladin.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.ranger.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.rogue.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.sorcerer.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.wizard.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.alchemist.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.cavalier.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 11) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 10) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.inquisitor.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.magus.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.oracle.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.summoner.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 9) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 8) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                if (self.parent.classes.witch.intValue > 0) {
                    if (hpRoll > 0 && hpRoll < 7) {
                        self.parent.character.currhp = [NSNumber numberWithInt:hpRoll];
                        self.parent.character.hp = [NSNumber numberWithInt:hpRoll];
                    } else {
                        int roll = (arc4random() % 6) + 1;
                        self.parent.character.currhp = [NSNumber numberWithInt:roll];
                        self.parent.character.hp = [NSNumber numberWithInt:roll];
                    }
                }
                self.parent.character.currhp = [NSNumber numberWithInt:self.parent.character.currhp.intValue + conMod];
                self.parent.character.hp = [NSNumber numberWithInt:self.parent.character.hp.intValue + conMod];
                [[self parent] saveContext];
                self.hpValues.text = [NSString stringWithFormat:@"%@ / %@", self.parent.character.currhp, self.parent.character.hp];
            }
            break;
        case 2:
            if (buttonIndex == 0) {
                NSString * value = expField.text;
                int hpLost = [value intValue];
                int currHP = self.parent.character.currhp.intValue - hpLost;
                if (currHP < 0) {
                    self.parent.character.currhp = [NSNumber numberWithInt:0];
                } else {
                    self.parent.character.currhp = [NSNumber numberWithInt:currHP];
                }
                [[self parent] saveContext];
                self.hpValues.text = [NSString stringWithFormat:@"%@ / %@", self.parent.character.currhp, self.parent.character.hp];
                CGFloat hpCurrProgress = (self.parent.character.currhp.floatValue / self.parent.character.hp.floatValue);
                if (hpCurrProgress > 1.0)
                    hpCurrProgress = 1.0;
                self.hpProgress.progress = hpCurrProgress;
                
            }
            if (buttonIndex == 1) {
                NSString * value = expField.text;
                int hpGained = [value intValue];
                int currHP = self.parent.character.currhp.intValue + hpGained;
                if (currHP > self.parent.character.hp.intValue) {
                    self.parent.character.currhp = self.parent.character.hp;
                } else {
                    self.parent.character.currhp = [NSNumber numberWithInt:currHP];
                }
                [[self parent] saveContext];
                self.hpValues.text = [NSString stringWithFormat:@"%@ / %@", self.parent.character.currhp, self.parent.character.hp];
                CGFloat hpCurrProgress = (self.parent.character.currhp.floatValue / self.parent.character.hp.floatValue);
                if (hpCurrProgress > 1.0)
                    hpCurrProgress = 1.0;
                self.hpProgress.progress = hpCurrProgress;
            }
            break;
            
        default:
            break;
    }
}

@end
