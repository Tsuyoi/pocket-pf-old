//
//  CharacterDetailViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "CharacterDetailViewController.h"

@implementation CharacterDetailViewController {
    NSString * ability;
    NSString * alignment;
    NSString * race;
    NSString * class;
    int strength;
    int dexterity;
    int constitution;
    int intelligence;
    int wisdom;
    int charisma;
    int strengthBonus;
    int dexterityBonus;
    int constitutionBonus;
    int intelligenceBonus;
    int wisdomBonus;
    int charismaBonus;
    int pointsRemaining;
    int pointsInitial;
    int alignmentIndex;
    int raceIndex;
    int classIndex;
}

@synthesize delegate;
@synthesize alignmentLabel;
@synthesize nameText;
@synthesize raceLabel;
@synthesize classLabel;
@synthesize abilityScoreLabel;
@synthesize strengthTotal;
@synthesize dexterityTotal;
@synthesize constitutionTotal;
@synthesize intelligenceTotal;
@synthesize wisdomTotal;
@synthesize charismaTotal;
@synthesize pointsTotal;
@synthesize genderSelect;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        ability = @"Strength";
        alignment = @"Chaotic Good";
        race = @"Dwarf";
        class = @"Barbarian";
        classIndex = 0;
        raceIndex = 0;
        alignmentIndex = 0;
        strength = 10;
        dexterity = 10;
        constitution = 10;
        intelligence = 10;
        wisdom = 10;
        charisma = 10;
        strengthBonus = 0;
        dexterityBonus = 0;
        constitutionBonus = 2;
        intelligenceBonus = 0;
        wisdomBonus = 2;
        charismaBonus = -2;
        pointsInitial = 20;
        pointsRemaining = pointsInitial;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)updateTotals
{
    self.strengthTotal.text = [NSString stringWithFormat:@"%d (%d Base / %d Bonus)", strength + strengthBonus, strength, strengthBonus];
    self.dexterityTotal.text = [NSString stringWithFormat:@"%d (%d Base / %d Bonus)", dexterity + dexterityBonus, dexterity, dexterityBonus];
    self.constitutionTotal.text = [NSString stringWithFormat:@"%d (%d Base / %d Bonus)", constitution + constitutionBonus, constitution, constitutionBonus];
    self.intelligenceTotal.text = [NSString stringWithFormat:@"%d (%d Base / %d Bonus)", intelligence + intelligenceBonus, intelligence, intelligenceBonus];
    self.wisdomTotal.text = [NSString stringWithFormat:@"%d (%d Base / %d Bonus)", wisdom + wisdomBonus, wisdom, wisdomBonus];
    self.charismaTotal.text = [NSString stringWithFormat:@"%d (%d Base / %d Bonus)", charisma + charismaBonus, charisma, charismaBonus];
    self.pointsTotal.text = [NSString stringWithFormat:@"%d / %d", pointsRemaining, pointsInitial];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.abilityScoreLabel.text = ability;
    self.alignmentLabel.text = alignment;
    self.raceLabel.text = race;
    self.classLabel.text = class;
    [self updateTotals];
}

- (void)viewDidUnload
{
    [self setAlignmentLabel:nil];
    [self setNameText:nil];
    [self setRaceLabel:nil];
    [self setClassLabel:nil];
    [self setAbilityScoreLabel:nil];
    [self setStrengthTotal:nil];
    [self setDexterityTotal:nil];
    [self setConstitutionTotal:nil];
    [self setIntelligenceTotal:nil];
    [self setWisdomTotal:nil];
    [self setCharismaTotal:nil];
    [self setPointsTotal:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)cancel:(id)sender {
    [self.delegate characterDetailViewControllerDidCancel:self];
}

- (IBAction)save:(id)sender {
    Character * character = [[Character alloc] init];
    character.name = nameText.text;
    character.raceLabel = race;
    character.classLabel = class;
    character.alignmentLabel = alignment;
    character.str = strength + strengthBonus;
    character.dex = dexterity + dexterityBonus;
    character.con = constitution + constitutionBonus;
    character.intel = intelligence + intelligenceBonus;
    character.wis = wisdom + wisdomBonus;
    character.cha = charisma + charismaBonus;
    character.alignment = alignmentIndex;
    character.race = raceIndex;
    character.cls = classIndex;
    character.gender = genderSelect.selectedSegmentIndex;
    [self.delegate characterDetailViewController:self didAddCharacter:character];
}

- (BOOL)canIncrease:(int)cost {
    if ((pointsRemaining - cost) < 0)
        return false;
    else
        return true;
}

- (int)calculateCost:(int)abilityScore :(int)stepperValue {
    int cost = 0;
    switch (abilityScore) {
        case 7:
            cost = 2;
            break;
        case 8:
            if (stepperValue == 7) {
                cost = -2;
            } else {
                cost = 1;
            }
            break;
        case 9:
            if (stepperValue == 8) {
                cost = -1;
            } else {
                cost = 1;
            }
            break;
        case 10:
            if (stepperValue == 9) {
                cost = -1;
            } else {
                cost = 1;
            }
            break;
        case 11:
            if (stepperValue == 10) {
                cost = -1;
            } else {
                cost = 1;
            }
            break;
        case 12:
            if (stepperValue == 11) {
                cost = -1;
            } else {
                cost = 1;
            }
            break;
        case 13:
            if (stepperValue == 12) {
                cost = -1;
            } else {
                cost = 2;
            }
            break;
        case 14:
            if (stepperValue == 13) {
                cost = -2;
            } else {
                cost = 2;
            }
            break;
        case 15:
            if (stepperValue == 14) {
                cost = -2;
            } else {
                cost = 3;
            }
            break;
        case 16:
            if (stepperValue == 15) {
                cost = -3;
            } else {
                cost = 3;
            }
            break;
        case 17:
            if (stepperValue == 16) {
                cost = -3;
            } else {
                cost = 4;
            }
            break;
        case 18:
            cost = -4;
            break;
    }
    return cost;
}

- (IBAction)updateStrength:(id)sender {
    UIStepper * stepper = (UIStepper *)sender;
    int cost = [self calculateCost:strength :stepper.value];
    if ([self canIncrease:cost]) {
        pointsRemaining -= cost;
        strength = stepper.value;
        [self updateTotals];
    } else {
        stepper.value = strength;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Not Enough Points!" message:[NSString stringWithFormat:@"You don't have enough points for this.\nPoints Left: %d\nPoints Needed: %d", pointsRemaining, cost] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)updateDexterity:(id)sender {
    UIStepper * stepper = (UIStepper *)sender;
    int cost = [self calculateCost:dexterity :stepper.value];
    if ([self canIncrease:cost]) {
        pointsRemaining -= cost;
        dexterity = stepper.value;
        [self updateTotals];
    } else {
        stepper.value = dexterity;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Not Enough Points!" message:[NSString stringWithFormat:@"You don't have enough points for this.\nPoints Left: %d\nPoints Needed: %d", pointsRemaining, cost] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)updateConstitution:(id)sender {
    UIStepper * stepper = (UIStepper *)sender;
    int cost = [self calculateCost:constitution :stepper.value];
    if ([self canIncrease:cost]) {
        pointsRemaining -= cost;
        constitution = stepper.value;
        [self updateTotals];
    } else {
        stepper.value = constitution;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Not Enough Points!" message:[NSString stringWithFormat:@"You don't have enough points for this.\nPoints Left: %d\nPoints Needed: %d", pointsRemaining, cost] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)updateIntelligence:(id)sender {
    UIStepper * stepper = (UIStepper *)sender;
    int cost = [self calculateCost:intelligence :stepper.value];
    if ([self canIncrease:cost]) {
        pointsRemaining -= cost;
        intelligence = stepper.value;
        [self updateTotals];
    } else {
        stepper.value = intelligence;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Not Enough Points!" message:[NSString stringWithFormat:@"You don't have enough points for this.\nPoints Left: %d\nPoints Needed: %d", pointsRemaining, cost] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)updateWisdom:(id)sender {
    UIStepper * stepper = (UIStepper *)sender;
    int cost = [self calculateCost:wisdom :stepper.value];
    if ([self canIncrease:cost]) {
        pointsRemaining -= cost;
        wisdom = stepper.value;
        [self updateTotals];
    } else {
        stepper.value = wisdom;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Not Enough Points!" message:[NSString stringWithFormat:@"You don't have enough points for this.\nPoints Left: %d\nPoints Needed: %d", pointsRemaining, cost] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)updateCharisma:(id)sender {
    UIStepper * stepper = (UIStepper *)sender;
    int cost = [self calculateCost:charisma :stepper.value];
    if ([self canIncrease:cost]) {
        pointsRemaining -= cost;
        charisma = stepper.value;
        [self updateTotals];
    } else {
        stepper.value = charisma;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Not Enough Points!" message:[NSString stringWithFormat:@"You don't have enough points for this.\nPoints Left: %d\nPoints Needed: %d", pointsRemaining, cost] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SelectAlignment"]) {
        AlignmentPickerViewController * alignmentPicker = segue.destinationViewController;
        alignmentPicker.delegate = self;
        alignmentPicker.alignment = alignment;
    }
    if ([segue.identifier isEqualToString:@"SelectRace"]) {
        RacePickerViewController * racePicker = segue.destinationViewController;
        racePicker.delegate = self;
        racePicker.race = race;
    }
    if ([segue.identifier isEqualToString:@"SelectClass"]) {
        ClassPickerViewController * classPicker = segue.destinationViewController;
        classPicker.delegate = self;
        classPicker.className = class;
    }
    if ([segue.identifier isEqualToString:@"SelectAbilityScore"]) {
        AbilityPickerViewController * abilityPicker = segue.destinationViewController;
        abilityPicker.delegate = self;
        abilityPicker.ability = ability;
    }
}

- (void)abilityPickerViewController:(AbilityPickerViewController *)controller didSelectAbility:(NSString *)abilityName {
    ability = abilityName;
    self.abilityScoreLabel.text = ability;
    if ([race isEqualToString:@"Half-Elf"] || [race isEqualToString:@"Half-Orc"] || [race isEqualToString:@"Human"]) {
        strengthBonus = 0;
        dexterityBonus = 0;
        constitutionBonus = 0;
        intelligenceBonus = 0;
        wisdomBonus = 0;
        charismaBonus = 0;
        if ([ability isEqualToString:@"Strength"])
            strengthBonus = 2;
        if ([ability isEqualToString:@"Dexterity"])
            dexterityBonus = 2;
        if ([ability isEqualToString:@"Constitution"])
            constitutionBonus = 2;
        if ([ability isEqualToString:@"Intelligence"])
            intelligenceBonus = 2;
        if ([ability isEqualToString:@"Wisdom"])
            wisdomBonus = 2;
        if ([ability isEqualToString:@"Charisma"])
            charismaBonus = 2;
    }
    [self updateTotals];    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)alignmentPickerViewController:(AlignmentPickerViewController *)controller didSelectAlignment:(NSString *)alignmentName alignmentIndex:(int)index{
    alignment = alignmentName;
    alignmentIndex = index;
    self.alignmentLabel.text = alignment;
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)racePickerViewController:(RacePickerViewController *)controller didSelectRace:(NSString *)raceName raceIndex:(int)index{
    race = raceName;
    raceIndex = index;
    self.raceLabel.text = race;
    if ([race isEqualToString:@"Dwarf"]) {
        strengthBonus = 0;
        dexterityBonus = 0;
        constitutionBonus = 2;
        intelligenceBonus = 0;
        wisdomBonus = 2;
        charismaBonus = -2;
    }
    if ([race isEqualToString:@"Elf"]) {
        strengthBonus = 0;
        dexterityBonus = 2;
        constitutionBonus = -2;
        intelligenceBonus = 2;
        wisdomBonus = 0;
        charismaBonus = 0;
    }
    if ([race isEqualToString:@"Gnome"]) {
        strengthBonus = -2;
        dexterityBonus = 0;
        constitutionBonus = 2;
        intelligenceBonus = 0;
        wisdomBonus = 0;
        charismaBonus = 2;
    }
    if ([race isEqualToString:@"Halfling"]) {
        strengthBonus = -2;
        dexterityBonus = 2;
        constitutionBonus = 0;
        intelligenceBonus = 0;
        wisdomBonus = 0;
        charismaBonus = 2;
    }
    if ([race isEqualToString:@"Half-Elf"] || [race isEqualToString:@"Half-Orc"] || [race isEqualToString:@"Human"]) {
        strengthBonus = 0;
        dexterityBonus = 0;
        constitutionBonus = 0;
        intelligenceBonus = 0;
        wisdomBonus = 0;
        charismaBonus = 0;
        if ([ability isEqualToString:@"Strength"])
            strengthBonus = 2;
        if ([ability isEqualToString:@"Dexterity"])
            dexterityBonus = 2;
        if ([ability isEqualToString:@"Constitution"])
            constitutionBonus = 2;
        if ([ability isEqualToString:@"Intelligence"])
            intelligenceBonus = 2;
        if ([ability isEqualToString:@"Wisdom"])
            wisdomBonus = 2;
        if ([ability isEqualToString:@"Charisma"])
            charismaBonus = 2;
    }
    [self updateTotals];
    [self.tableView reloadData];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:0,1,2,3,4,nil] withRowAnimation:UITableViewRowAnimationFade];
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)classPickerViewController:(ClassPickerViewController *)controller didSelectClass:(NSString *)className classIndex:(int)index{
    class = className;
    classIndex = index;
    self.classLabel.text = class;
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)didFinishEditing:(id)sender {
    [sender resignFirstResponder];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            if (race == @"Human" || race == @"Half-Elf" || race == @"Half-Orc")
                return 5;
            else
                return 4;
        case 1:
            return 1;
        case 2:
            return 7;
        default:
            return 0;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
