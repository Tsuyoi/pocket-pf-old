//
//  CharacterListCell.h
//  Pocket PF
//
//  Created by Caylin Hickey on 2/11/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CharacterListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel * nameLabel;
@property (strong, nonatomic) IBOutlet UILabel * descriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView * classImage;

@end
