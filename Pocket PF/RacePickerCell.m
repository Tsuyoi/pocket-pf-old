//
//  RacePickerCell.m
//  Pocket PF
//
//  Created by Caylin Hickey on 3/29/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "RacePickerCell.h"

@implementation RacePickerCell

@synthesize lblRaceName;
@synthesize imgRaceIcon;

@end
