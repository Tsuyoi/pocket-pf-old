//
//  CharacterData.m
//  Pocket PF
//
//  Created by Caylin Hickey on 5/1/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "CharacterData.h"
#import "ClassData.h"
#import "FeatData.h"
#import "MagicItemData.h"
#import "SkillData.h"
#import "SpellData.h"
#import "WeaponData.h"


@implementation CharacterData

@dynamic age;
@dynamic alignment;
@dynamic cha;
@dynamic cls;
@dynamic con;
@dynamic currhp;
@dynamic deity;
@dynamic dex;
@dynamic exp;
@dynamic eyes;
@dynamic female;
@dynamic gold;
@dynamic hair;
@dynamic height;
@dynamic homeland;
@dynamic hp;
@dynamic intel;
@dynamic name;
@dynamic race;
@dynamic str;
@dynamic weight;
@dynamic wis;
@dynamic classes;
@dynamic feats;
@dynamic magicitems;
@dynamic skills;
@dynamic spells;
@dynamic weapons;

@end
