//
//  ClassPickerViewController.m
//  Pocket PF
//
//  Created by Caylin Hickey on 2/13/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import "ClassPickerViewController.h"
#import "ClassPickerCell.h"

@implementation ClassPickerViewController {
    NSArray * coreClasses;
    NSArray * baseClasses;
    NSUInteger selectedSection;
    NSUInteger selectedRow;
}

@synthesize delegate;
@synthesize className;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    baseClasses = [NSArray arrayWithObjects:
                   @"Barbarian",
                   @"Bard",
                   @"Cleric",
                   @"Druid",
                   @"Fighter",
                   @"Monk",
                   @"Paladin",
                   @"Ranger",
                   @"Rogue",
                   @"Sorcerer",
                   @"Wizard",
                   nil];
    
    coreClasses = [NSArray arrayWithObjects:
                   @"Alchemist",
                   @"Cavalier",
                   @"Inquisitor",
                   @"Magus",
                   @"Oracle",
                   @"Summoner",
                   @"Witch",
                   nil];
    if ([baseClasses containsObject:className]) {
        selectedSection = 0;
        selectedRow = [baseClasses indexOfObject:className];
    } else {
        selectedSection = 1;
        selectedRow = [coreClasses indexOfObject:className];
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Base Classes";
            break;
        case 1:
            return @"Core Classes";
            break;
        default:
            return @"Section";
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [baseClasses count];
            break;
        case 1:
            return [coreClasses count];
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClassPickerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClassCell"];
    NSString * cls;
    if (indexPath.section == 0)
        cls = [baseClasses objectAtIndex:indexPath.row];
    if (indexPath.section == 1)
        cls = [coreClasses objectAtIndex:indexPath.row];
    NSString * img = [NSString stringWithFormat:@"%@_Icon.png", cls];
    cell.lblClassName.text = cls;
    cell.imgClassImage.image = [UIImage imageNamed:img];
    if (indexPath.section == selectedSection && indexPath.row == selectedRow)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    if (selectedRow != NSNotFound) {
        UITableViewCell * cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:selectedSection]];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    selectedRow = indexPath.row;
    selectedSection = indexPath.section;
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    NSString * selectedClass = [[NSString alloc] init];
    if (indexPath.section == 0)
        selectedClass = [baseClasses objectAtIndex:indexPath.row];
    else
        selectedClass = [coreClasses objectAtIndex:indexPath.row];
    int classIndex = indexPath.section * 11 + indexPath.row;
    if (classIndex > 12)
        classIndex++;
    [self.delegate classPickerViewController:self didSelectClass:selectedClass classIndex:classIndex];
}

@end
