//
//  CharacterSpellTableViewController.h
//  Pocket PF
//
//  Created by Caylin Hickey on 4/7/12.
//  Copyright (c) 2012 TsuyoiSoft, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CharacterInfoTabBarController.h"
#import "SpellSource.h"

@interface CharacterSpellTableViewController : UITableViewController

@property (nonatomic, strong) CharacterInfoTabBarController * parent;
@property (assign, nonatomic) NSInteger charIndex;
@property (nonatomic, strong) NSMutableArray * spellList;

@end
